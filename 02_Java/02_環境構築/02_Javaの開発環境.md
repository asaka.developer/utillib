# **Javaの開発環境構築**

## **目次**

- [**1.概要**](#1概要)
- [**2.手順**](#2手順)
  - [**2.1.Visutal Studio Codeのインストール**](#21visutal-studio-codeのインストール)
  - [**2.2.Visual Studio Codeのセットアップ**](#22visual-studio-codeのセットアップ)
  - [**2.3.Java用のプラグインをインストール**](#23java用のプラグインをインストール)

---

## **1.概要**

開発環境としてVisual Studio Codeを使用します。  
本資料では、Visual Studio Codeの導入方法を解説致します。

---

## **2.手順**

### **2.1.Visutal Studio Codeのインストール**

1. [ここ](https://code.visualstudio.com/download)をクリックし、Visual Studio CodeをDownloadする。  
   <img src="images/02_vscode/2.1.1.png" width="350">  

2. ダウンロードが完了したら、インストーラーを起動する。  

3. 同意確認の画面で「同意する(A)」を選択し、「次へ」をクリックする。  
   <img src="images/02_vscode/2.1.3.png" width="350"> 

4. インストール先指定の画面で「次へ」をクリックする。  
   <img src="images/02_vscode/2.1.4.png" width="350"> 

5. スタートメニューフォルダーの指定で「次へ」をクリックする。  
   <img src="images/02_vscode/2.1.5.png" width="350"> 

6. 「デスクトップ上にアイコンを作成する」「サポートされているファイルの種類のエディタとして、Codeを登録する」、  
   「PATHへの追加」にチェックを入れ、「次へ」をクリックする。  
   <img src="images/02_vscode/2.1.6.png" width="350">

7. インストール準備完了画面で「インストール」をクリックする。  
   <img src="images/02_vscode/2.1.7.png" width="350">

8. インストールの完了を待つ。  
   <img src="images/02_vscode/2.1.8.png" width="350">

9. 「Visual Studio Codeを実行する」にチェックを入れ、「完了」をクリックする。  
   <img src="images/02_vscode/2.1.9.png" width="350">

### **2.2.Visual Studio Codeのセットアップ**

1. Visual Studio Codeを起動したら、言語パックのポップアップの「インストールして再起動」をクリックする。  
   <img src="images/02_vscode/2.2.1.png" width="350">

2. Visual Studio Codeが再起動したら、好きなテーマの色を選択する。  
   （資料はダークの色で作成をしております。）  
   <img src="images/02_vscode/2.2.2.png" width="350">

---

### **2.3.Java用のプラグインをインストール**

1. 左メニューの一番下をクリックし、拡張機能のメニューを表示する。  
   <img src="images/02_vscode/2.3.1.png" width="350">

2. 検索欄に「Extension Pack for Java」と入力する。  
   <img src="images/02_vscode/2.3.2.png" width="350">

3. 検索結果から「Extension Pack for Java」を選択し、「インストール」をクリックする。  
   <img src="images/02_vscode/2.3.3.png" width="350">

4. Get Started with Java Developmentの画面が表示されますが、  
   JDKは既にインストール済なので対応不要です。  
   <img src="images/02_vscode/2.3.4.png" width="350">

5. 検索欄に「Debugger for Java」と入力する。  
   <img src="images/02_vscode/2.3.5.png" width="350">

6. 検索結果から「Debugger for Java」を選択し、「インストール」をクリックする。  
   <img src="images/02_vscode/2.3.6.png" width="350">

7. インストールが完了したら「×」でVisual Studio Codeを閉じる。  
   <img src="images/02_vscode/2.3.7.png" width="350">



