# **JDKのインストール・環境変数の設定**

## **目次**

- [**1.概要**](#1概要)
- [**2.手順**](#2手順)
  - [**2.1.JDKのインストール**](#21jdkのインストール)
  - [**2.2.JDKの環境変数を設定**](#22jdkの環境変数を設定)

---

## **1.概要**

Javaでプログラミングを行う際はJDK(Java SE Development Kit)を使用し、開発を行います。
本ドキュメントではJDKのインストール、環境変数の設定を行います。

---

## **2.手順**

### **2.1.JDKのインストール**

[JDKのダウンロード、及びインストール](https://www.oracle.com/technetwork/java/javase/downloads/index.html)を実施します。  
https://www.oracle.com/technetwork/java/javase/downloads/index.html    

1. JDK Downloadを選択します。  
   <img src="images/01_create_enviroment/2.1.1.png" width="350">
2. PCのOSに応じたファイルを選択します。  
   <img src="images/01_create_enviroment/2.1.2.png" width="350">  
   - Windows 64bit の場合: `Windows x64 Installer`のインストーラーファイルをダウンロード
   - macOS 64bit の場合: `macOS Installer`のインストーラーファイルをダウンロード
3. 規約に同意し、ダウンロードボタンをクリックします。  
   <img src="images/01_create_enviroment/2.1.3.png" width="350">  
4. ダウンロードしたインストーラーファイルを実行します。
5. 次へをクリックします。  
   <img src="images/01_create_enviroment/2.1.5.1.png" width="350">  
 
6. 宛先フォルダのPathをメモ帳にコピペします。  
   その後、メモ帳にコピペしたPathの後ろに「\bin」を付けます。  
   ※ これが`JDKのPath`です。  
   ※ ファイルの数値(バージョン)は、ダウンロードしたバージョンに読み替えてください。  
   下記の図だと「`C:\Program Files\Java\jdk-14.0.1\bin`」が環境変数のPathです。  
   Pathのコピーが完了後、「次へ」をクリックします。  
   <img src="images/01_create_enviroment/2.1.5.2.png" width="350">  
7. インストールが完了したら、閉じるをクリックします。  
   <img src="images/01_create_enviroment/2.1.6.png" width="350">  

### **2.2.JDKの環境変数を設定**

ダウンロードしたJDKの環境変数を設定します。  

1. コントローラーパネルを開きます。
2. システムとセキュリティを選択します。  
   <img src="images/01_create_enviroment/2.2.2.png" width="350">  
3. システムを選択します。  
   <img src="images/01_create_enviroment/2.2.3.png" width="350">  
4. システムの詳細設定を選択します。  
   <img src="images/01_create_enviroment/2.2.4.png" width="350">  
5. 環境変数を選択します。  
   <img src="images/01_create_enviroment/2.2.5.png" width="350">  
6. システム環境変数の一覧からPathをダブルクリックします。  
   <img src="images/01_create_enviroment/2.2.6.png" width="350">  
7. 「新規」をクリックし、JDKのpathを入力します。  
   <img src="images/01_create_enviroment/2.2.7.1.png" width="350">  
   <img src="images/01_create_enviroment/2.2.7.2.png" width="350">  
8. 「OK」をクリックし、環境変数名の編集画面を閉じます。  
   <img src="images/01_create_enviroment/2.2.8.png" width="350">  
9. 「OK」をクリックし、環境変数画面を閉じます。  
   <img src="images/01_create_enviroment/2.2.9.png" width="350">  
10. 「適応」をクリックし、システムのプロパティに反映させます。  
   <img src="images/01_create_enviroment/2.2.10.png" width="350">  
