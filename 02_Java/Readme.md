# Readme

## 概要

Javaの入門ドキュメントを作成。

## 詳細

- 1.プログラミングとは(1h)
  - 1.1.プログラミングとは何かの説明
  - 1.2.2進数の説明
    - 1.1.1.コンピュータは0と1しか分からない
    - 1.1.2.OSの違いによる影響についての説明 
  - 1.2.Buildについての説明
    - 1.2.1.人間が書いたプログラムは0と1に変換される旨の説明
    - 1.2.2.コンパイル言語とインタプリンタ言語についての説明
  - 1.3.開発環境についての説明
    - メモ帳でプログラムを作成できる旨を説明。
  - 1.4.質問事項の受付  
- 2.Javaの環境構築について(0.5h)
- 3.開発環境の構築(0.5h)
    - 1.Visual Studio Codeのインストール
    - 2.Extentionのインストール（Extention Pack for Java）
- 4.プログラミング入門（Java）
  - メモ帳(1.5h)
    - javaファイルの作成(0.5h)
      - Javaファイルを作成
      - Hellow World表示用プログラムの実装
    - コンパイルと実行(0.25h)
      - コマンドプロンプト
        - ディレクトリの説明と移動
        - javacでのコンパイル
        - Hellow Worldの表示
    - 練習問題(0.25h)
      - 文字の変更
      - 文字の追加
  - Visual Studio Code(0.25h)
      - 作成したJavaファイルをVisual Studio Codeで読み込み
      - Visual Studio Codeでの実行
      - 仕組みの説明（拡張プラグインでコンパイルと実行している旨の説明）  
  - 変数について(1h)
      - 変数の仕組み
      - 文字列
      - 数値
      - 計算
      - 練習問題(print)
  - If文(0.5h)
      - If文の学習
      - 練習問題
      - 答え合わせ
  - For文(1.0h)
      - For文の学習
      - 練習問題
      - 答え合わせ
  - 


1, 2は午前
3以降は午後

## 参考資料

- [OS差分吸収についての資料](https://kanda-it-school-kensyu.com/java-basic-intro-contents/jbi_ch01/jbi_0102/#:~:text=Java%E3%81%AF%E3%80%81%E3%82%B3%E3%83%B3%E3%83%91%E3%82%A4%E3%83%A9%E5%9E%8B%E8%A8%80%E8%AA%9E,%E5%8B%95%E3%81%8B%E3%81%9B%E3%82%8B%E3%82%88%E3%81%86%E3%81%AB%E3%81%AA%E3%82%8A%E3%81%BE%E3%81%99%E3%80%82)
- [コンパイルとBuildの違い](https://wa3.i-3-i.info/diff502programming.html#:~:text=%E3%81%93%E3%81%AE%E8%A9%B1%E3%81%AB%E3%81%8A%E3%81%84%E3%81%A6%E3%80%81%E3%81%9D%E3%82%8C%E3%81%9E%E3%82%8C%E3%81%AE,%E4%BD%9C%E3%82%8A%E4%B8%8A%E3%81%92%E3%82%8B%E4%BD%9C%E6%A5%AD%E3%81%8C%E3%83%93%E3%83%AB%E3%83%89%E3%81%A7%E3%81%99%E3%80%82)

## 備考

10:00 ～ 18:00

| 時間          | 内容                                                                           |
| ------------- | ------------------------------------------------------------------------------ |
| 10:00 ~ 10:10 | 点呼と挨拶                                                                     |
| 10:10 ~ 11:00 | プログラミングとは（座学）                                                     |
| 11:00 ~ 12:00 | 開発環境構築                                                                   |
| 12:00 ~ 13:00 | 休憩                                                                           |
| 13:00 ~ 13:50 | メモ帳_Hellow Worldの作成(20分)_cmd操作(10分)_build(10分)_error説明(10分)      |
| 13:50 ~ 14:00 | Visual Studio Code_エディタの操作とBuild(10分) ※以降はVisual Studio Codeで開発 |
| 14:00 ~ 14:40 | 変数の説明(15分)・練習問題(15分)・答え合わせ(10分)                             |
| 14:40 ~ 14:50 | 休憩                                                                           |
| 14:50 ~ 15:20 | 四則演算(10分)・練習問題(10分)・答え合わせ(10分)                               |
| 15:20 ~ 15:50 | キーボードからの入力(10分)・練習問題(10分)・答え合わせ(10分)                   |
| 15:50 ~ 16:20 | IF文の説明(10分)・練習問題(10分)・答え合わせ(10分)                             |
| 16:20 ~ 16:30 | 休憩                                                                           |
| 16:30 ~ 17:00 | 配列の説明(10分)・練習問題(10分)・答え合わせ(10分)                             |
| 17:00 ~ 17:30 | For文の説明(10分)・練習問題(10分)・答え合わせ(10分)                            |
| 17:30 ~ 17:50 | 総合練習問題(10分)・答え合わせ(10分)                                           |
| 17:50 ~ 18:00 | 閉幕の挨拶とアンケート                                                         |