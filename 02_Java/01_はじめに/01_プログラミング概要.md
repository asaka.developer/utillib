# **プログラミング概要**

## **目次**

- [**1.概要**](#1概要)
- [**2.コンピュータについて**](#2コンピュータについて)
  - [**2.1.コンピュータの世界**](#21コンピュータの世界)
  - [**2.2.0と1の世界**](#220と1の世界)
  - [**2.3.数字**](#23数字)
  - [**2.4.文字**](#24文字)
- [**3.プログラミングについて**](#3プログラミングについて)
  - [**3.1.プログラミングとは**](#31プログラミングとは)
  - [**3.2.人間が書いたプログラムは0と1に変換される**](#32人間が書いたプログラムは0と1に変換される)
  - [**3.3.OSについて**](#33osについて)
  - [**3.4.本研修で学習するJava言語について**](#34本研修で学習するjava言語について)
- [**4.開発環境について**](#4開発環境について)
  - [**4.1.JDKについて**](#41jdkについて)
  - [**4.2.開発環境について**](#42開発環境について)

---

## **1.概要**

本ドキュメントではコンピュータについて理解を深めていただきます。  
コンピュータについて知ることで、午後から実施するプログラミングについて  
より理解を深めることができます。  
  
本講座ではプログラミングの学習を目的としておりますので、  
座学は軽く流す形で進めます。  
コンピュータについて理解するには、本研修の時間では足りませんので、  
皆さんは、「コンピュータってそういう物なんだな」程度の感覚で難しく考えず、  
お聞き下さい。  

---

## **2.コンピュータについて**

### **2.1.コンピュータの世界**

私たちは日々、スマートフォンやパソコンなどのコンピュータを使用しています。  
例えばメールでは、日本語や英語など、`人間が理解できる文字`でミュニケーションをとっていますね。  
SNSでは`文字`や`画像`を発信し、YouTubeなどの配信サービスでは`動画`を閲覧できます。  

ではコンピュータは、何の言葉を使用しているのでしょうか？  
そもそも言葉を使用してるのでしょうか？  
答えは`機械語`を使用しています。

皆様は`機械語`という言葉を聞いたことがあるでしょうか？  
そもそも機械語とはどのような発音なのでしょうか？  
実は`機械語は言葉ではない`のです。  
コンピュータは`0`と`1`しか分かりません。
日本語や英語など、人間の言葉は理解できません。  
そのため、コンピュータはすべて`0`と`1`の羅列でコミュニケーションをとっています。  
  
コンピュータは`0`と`1`しか分からない、このことを知っておくかどうかで、今後のIT社会における、ITに対する理解度が大きく変化することになります。  

---

### **2.2.0と1の世界**
  
皆さん、疑問には思いませんか？  
私たちは日本語や英語など、人間が認識できる言葉をコンピュータに入力して使用しているはずです。  
それなのに何故、コンピュータは日本語や英語をメールやSNSなどで使用できているのでしょうか？  
  
答えは簡単です。  
日本語や英語など、人間に分かる言葉を`0`と`1`に置き換えているからです。  

---

### **2.3.数字**

皆さんは、私たちが普段使用している数字について深く考えたことはありますでしょうか。 
日所生活では`1桁は0~9`までの数値を使用してますよね。  
  
例えば、〇は何個書いてるでしょうか？  
〇〇〇 → 3個ですよね。  
  
では下の場合はどうでしょうか？  
〇〇〇〇〇〇〇〇〇〇〇〇 → 12個ですよね。  
9の次の数値が表せないから、桁を上げてまた`0~9`を数えてますよね。  
つまりは`0~9の数値(10通り)`の数値のため`10進数`となります。  
  
`0~1`の場合はどうでしょう？  
0~1の場合は2通りしか表せないですよね。  
コンピュータは2進数を使用していることになります。  
では〇〇〇(3個)を表現するとき、どのように表現するのでしょうか？  
  
答えは11となります。  
コンピュータは`0~1`しか表現できないため、`1桁で2通り`しか表現できません。  
そのため、3の場合は桁上がりをするため、`11`となります。  

これ以上詳しい内容は、今回の講習の範囲ではありませんので、  
皆さんは`コンピュータは0~1しか表現できない`ということだけ覚えておいて下さい。

---

### **2.4.文字**

コンピュータは`0`と`1`しか理解できません。  
そのため、例えばアルファベットのAを表現するときには  
`01000001`のように2進数で表現し、  
アルファベットのBを表現する時には
`01000010`と2進数で表現します。

2進数の値は文字コード毎にことなりますので、割愛致します。  
皆さんは、`コンピュータは文字を表現するときに0と1に置き換えている`ということだけ覚えておいて下さい。

---

## **3.プログラミングについて**

### **3.1.プログラミングとは**

プログラマーはC言語、Java言語など、プログラミング言語を使用し開発を行います。  
プログラミング言語は多く、用途によってことなります。  
例えば組み込み系では、動作の軽いC言語が使用されることが多いです。  
大きなシステム系では、安全性の高いJava言語が使用されることが多いです。  
  
皆さんは`プログラミング言語は沢山ある`と覚えておいてください。

---

### **3.2.人間が書いたプログラムは0と1に変換される**

プログラミング言語は人間によって作成されています。  
ではプログラミング言語で書いたソースコードを、コンピュータは理解できるのでしょうか？  
`答えはNo`です。コンピュータは`0`と`1`しか理解できません。  
そのため、C言語、Java言語といったプログラミング言語を、    
コンピュータが理解できる`0`と`1`に置き換えることで、  
コンピュータに動作を行わせています。  
この変換を`コンパイル`と言います。
  
皆さんは`開発されたプログラムは0と1にコンパイルして、コンピュータが理解できている`と覚えておいて下さい。

---

### **3.3.OSについて**

OSによっては理解できないプログラミング言語がございます。  
例えばスマートフォンのiPhoneやiPadはAppleによって開発されたiOSで動作しています。  
iOSのアプリはObjective-C、Swiftといった言語で開発されています。  
ですが、Java言語は動作しません。  
  
逆にAndroidのアプリはJava言語、Kotlinといった言語で開発されています。  
ですが、Objective-CやSwiftはAndroidでは動作しません。  
  
上記の理由は`OSによって機械語のルールが異なる`からです。  
皆さんは`OSによって理解できるプログラミング言語は異なる`と覚えておいて下さい。
  
---

### **3.4.本研修で学習するJava言語について**

Java言語はJVM(Java Virtual Machine)で動作しています。  
  
上記で記載した通り、OSによって理解できるプログラミング言語はことなります。  
そのため、OSが理解できるプログラミング言語で実装する必用があります。  
ですが、OSごとに違うプログラミング言語を使用するのは大変ですよね。  
  
そこで`JVM`の登場です。  
`Java言語`をコンパイルすることで、`中間言語(0と1)`に変換しています。(`classファイル`)  
`JVM`は各OSにインストールすることで、Javaの実行環境を作成します。  
Java言語のコンパイルによって作成されては中間言語は、JVM上で動作します。  
そのため、どのOSでも同じように理解し、動作することができます。  

皆さんは`Javaで開発されたプログラムは、JVMがインストールされたコンピュータであれば、でどのOSでも同じように動作する`と覚えておいて下さい。

---

## **4.開発環境について**

プログラミングを行う際は開発キットを使用します。  
Java言語の場合はJDK(Java Development Kit)を使用します。  

家具を作る際には金槌、ドライバー、釘、ネジなどを使用しますよね。  
金槌、ドライバー、釘、ネジなど、物作りに使用する作成キットを想像いただくと、  
イメージがつきやすいと思います。

---

### **4.1.JDKについて**

JDKには、Javaでプログラミングを作るための道具と、  
Javaで作成したプログラムを動かす道具の一式が含まれています。  

---

### **4.2.開発環境について**

プログラミングを行う際は、Windowsに入っているメモ帳など、  
文字がかけるエディタで開発を行うことができます。  
ですが、メモ帳はプログラミングすることを目的に作成されていないため、  
プログラミングする際には不便な点があります。  

そこで、`統合開発環境(IDE)`の出番です。  
統合開発環境には、プログラミングを行う際に便利なツールや  
補完機能が搭載されています。  
また、自身でプラグインを導入することで、色々なツールを  
追加する事ができます。  
そのため、効率よく開発することができます。  

本講座では初めにメモ帳でのプログラミング、  
その後、統合開発環境を使用して皆様にプログラミングを体験していただきます。
