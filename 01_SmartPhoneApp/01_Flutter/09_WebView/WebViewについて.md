# WebViewについて

FlutterにはWebViewのプラグインが複数存在する。  
公式のプラグインはバグが多くて使用できないため、非公式のプラグインを使用する事になる。  

- [WebView](https://pub.dev/packages/webview_flutter) → バグが多すぎて使えない
- [WebviewScaffold](https://pub.dev/packages/flutter_webview_plugin) → 一番安定している
- [inappwebview](https://pub.dev/packages/flutter_inappwebview) → キーボードに不具合あり

## **HTML内のアンカーテキストについて**

既存のWebViewのPluginでは、HTML内のアンカーテキストをサポートしていない。  
そのため、プラグインの中身を自身で修正する必要がある。（Flutter側・Native側の両方）  
尚、アンカーテキストの取得はNative側の`HitTestResult`で取得できる。

### **修正のヒント**

WebviewScaffoldの場合 (Android編)  

- 1.`webview_scaffold.dart`から`FlutterWebviewPlugin.java`が呼ばれる。
- 2.`FlutterWebviewPlugin.java`から`WebviewManager.java`が呼ばれる。
    - `WebviewManager.java`でWebViewを長押しした際に、Flutter側に`HitTestResult`を渡す処理を実装する必要がある。
        - `FlutterWebviewPlugin.channel.invokeMethod("hitTestResult", url);`みたいな感じでFlutterに渡す。
    - https://bg1.hatenablog.com/entry/2017/09/13/210000
    - http://android-note.open-memo.net/sub/web_view__get_src_type.html
    - https://groups.google.com/forum/#!topic/android-group-japan/i8C4uFocUms
- 3.Flutter側で`HitTestResult`を受け取る処理を実装する必要がある。（FlutterWebviewPlugin）
    - `FlutterWebviewPluginクラス`の`_handleMessagesメソッド`に実装する。