// ファイルを扱うライブラリ
import 'dart:io';
// バイナリデータを扱うライブラリ
import 'dart:typed_data';
// ファイルを選択するライブラリ
import 'package:file_picker/file_picker.dart';
// カメラの起動、ギャラリを起動するライブラリ
import 'package:image_picker/image_picker.dart';
// アプリ内領域、外部ストレージのパスを取得するライブラリ
import 'package:path_provider/path_provider.dart';

class Example {

  void openCamera(String fileName) async {
    // カメラを起動する（動画）
    PickedFile pickedFile = await imagePicker.getVideo(source: ImageSource.camera);
    // カメラで録画した動画のバイナリデータを取得する
    saveVideo(await pickedFile.readAsBytes(), fileName);
  }

  void selectVideo(String fileName) async {
    // ファイル選択画面を起動する（動画）
    File pickedFile = await FilePicker.getFile(type: FileType.video);
    // 選択した動画のバイナリデータを取得する
    saveVideo(await pickedFile.readAsBytes(), fileName);
  }

  void saveVideo(Uint8List bytes, String fileName) async {
    // OSによって保存先や処理が異なる
    if (Platform.isAndroid) {
      saveVideoForAndroid(bytes, fileName);
    } else if (Platform.isIOS) {
      saveVideoForIOS();
    }
  }

  void saveVideoForAndroid(Uint8List bytes, String fileName) async {
    // アプリ内領域のディレクトリパスを取得する
    final Directory extDir = await getApplicationDocumentsDirectory();
    // 保存先のディレクトリパスを設定する
    final String dirPath = '${extDir.path}/Video/flutter_test';
    // 保存先のディレクトリを作成する
    await Directory(dirPath).create(recursive: true);
    // ファイルを作成する
    final String filePath = '$dirPath/$fileName.mp4';
    final File appFile = File(filePath);
    // 作成したファイルにバイナリデータを書き込む
    await appFile.writeAsBytes(bytes);
  }

  void saveVideoForIOS() async {}
}