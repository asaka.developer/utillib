# Flutterでのアプリ開発所感

## **環境構築**

- 頻繁に開発環境の更新を要求される
    - Flutter SDKのUpgrade
    - Dart言語のUpgrade
    - MacOSのアップグレード
        - Versionが古いと、Xcodeを最新にできない
    - Xcodeのアップグレード
        - Versionが古いと、FlutterからUpgrade要求が表示されBuildができない

---

## **Build**

- Windowsでアプリを作成し、MacでBuildすると、Flutter SDKが違う旨のエラーが表示されBuildできないことがある。
    - Flutter SDKを再インストールすると、Buildできるようになる。
    - 本事象の根本的な原因は調査中

---

## **複雑な構成のアプリ**

- 複雑な機能のアプリを作成する際は、Fltterでは実現できない機能が多い。
- その際は、FlutterからNativeのコードを呼び出し対処する必要がある。
- しかしながら、Nativeのコード呼び出しは、各プラットフォームでの実装と同義であるため、Flutterを使用する意義が無くなる。
- そのため、`複雑な機能のアプリ`はFlutterを使用せず、`完全にNativeで開発`する方が良い。
  ```text
  WebViewでページの読み込みが完了した際に、表示先のurlを取得したいが画面表示されてしまってから4秒後にCallbackが呼ばれる劇遅動作となる。  
  また、Plugin自体にも不具合が多く、キーボードを1文字入力しただけで決定される不具合もある。  
  修正の目途は立っていない。
  ```

---

## **WebViewについて**

- Flutterには`WebView`と`WebviewScaffold`が存在する。
    - javascriptの有効・無効の切換えはできる
    - Dom Storageの有効・無効の切換えはできない

### **1.動画の再生**

- 不具合
    - Flutterの`WebView`と`WebviewScaffold`には、iOSで不具合が発生する。
    - 動画を直接読み込むと、iOSではWebViewで動画の再生が勝手に始まる。
    - また、iOSで動画の再生を停止すると、WebViewの読み込みでデッドロックが発生し、アプリが操作不能になる。
    - Androidでは上記不具合は発生しない。
- 原因
    - AndroidはWebView, iOSはWKWebViewを使用しているが、OSの仕様・ブラウザの仕様が影響しているかもしれない。
- 対応
  - Flutterの`WebView`と`WebviewScaffold`で動画を再生する場合、Html形式で読み込ませると不具合は発生しない。
  - 以下のようなhtmlファイルをプログラム上で作成し、読み込ませることで解決する。
  - （スタジアムナビを参考にして、htmlを作成する）

```html
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0,maximum-scale=1.0,user-scalable=0">
    </head>
    <body>
        <video src="$highlightUrl" width="100%" poster="$thumbnail" onclick="this.play()" controls="">
            <p>動画を再生するにはvideoタグをサポートしたブラウザが必要です。</p>
        </video>
    </body>
</html>
```

### **2.WebViewの切換え**

- 不具合
    - Widget上で`WebViewA`から`WebViewB`に切り替えても、表示が`WebViewA`のままになっている。
- 原因
    - Flutterの仕様、または不具合。
    - WebViewは、同じクラスのWidget同士を入れ替えると、更新されなかった。(StatefullWidget)
    - Textは、同じクラスのWidget同士を入れ替えても更新された。(StatelessWidget)
    - Image（StatefullWidget）では再現しなかったため、WebView特有の問題の可能性がある。
- 対応
    - 一度、WebViewのWidgetを取り除き、改めてWebViewのWidgetをAttachすると表示された。
    - そのため、WebView(変更前) → Text(差し替え) → WebView(変更後)の順でインスタンスを入れ替えると解決する。

---

## **iOSの対応について**

- iOSの実機にアプリをインストールする場合、Tergetとなる端末のプロビジョニングファイルが必要となる。

---

## **タブレット端末の対応について**

- スマホを想定して作成したアプリは、タブレットに最適化されていない。
    - 画面サイズを計測し、スマホ・タブレットを判定させる。
    - それぞれの種類に応じたUIの実装をする必要がある。
    - アプリ自体はタブレットでも動作する。
    - 例）WebViewの場合はモバイル版ではなく、PC版を表示する。  
      　　各種ボタンや文字は拡大をする。

## **非同期処理について**

### **async await**

- 非同期処理はasync awaitで待つ。
- Thenを使っても良いがネストが深くなってしまうので注意

### **FutureBuilder**

- FutureBuilderは非同期処理の完了を待ち、非同期処理が完了後にWidgetを作成する事ができる。

- ~~ただし、原因は不明だがFutureBuilderで非同期処理を行うと、完了後の処理が2回呼ばれている。~~
    - ~~StatefulWidgetでFutureBuilderを使用する場合、initState時点で非同期処理を開始し、FutureBuilderからその処理を読み込むようにする必要がある~~
    - ~~Buildメソッド内のFutureBuilderで非同期処理を実行すると、再度親WidgetがBuildされてFutureBuilderが実行されてしまう。~~
    - ~~（ログ上での見た範囲なので、仕様とは断言できないがFutureは完了前、完了後の2回が呼ばれている）~~
- ~~https://translate.google.com/translate?hl=ja&sl=en&u=https://stackoverflow.com/questions/58664293/futurebuilder-runs-twice&prev=search&pto=aue~~
- ~~https://api.flutter.dev/flutter/widgets/FutureBuilder-class.html~~

**`if(snapshot.connectionState == ConnectionState.done) {}`の時に成功処理を記述すると、本問題は解決する！！**

#### **原因**

- FutureBuilderの`子Widget`で、FutureBuilderを持つ`親Widget`を再Buildすると、前のBuild結果を保持したままになっている。  
  そのためsnapshotのdataはnullになっていない。  
  これにより、上記の完了後2回が呼ばれている状態となっている。
  対策としては、再Buildを行う前提でのFutureBuilderの使用を避ける、もしくはProviderやBlocを使用するしてFutureBuilderの代替え処理を実装する。

    ```dart
    // Google推奨 StatefulWidgetにする
    class Example extends StatefulWidget {
      final Model model;
      final String refreshToken;
      Future<String> futureSso;
      @override
      _ExampleState createState() => _ExampleState();
    }

    class _ExampleState extends State<Example> {
      Future<String> _futureRefreshToken;

      @override
      void initState() {
        super.initState();
        // StateのinitStateで非同期処理を実行
        _futureRefreshToken = SSO().getSsoToken(widget.model, widget.refreshToken);
      }

      @override
      Widget build(BuildContext context) {
        return FutureBuilder(
          future: _futureRefreshToken, // 非同期処理実施中の変数をセット
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              // 非同期処理完了後
              return Text("");
            } else {
              // 非同期処理完了前
              return Center(child: CircularProgressIndicator());
            }
          },
        );
      }
    }
    ```

    ```dart
    // Google非推奨（独自実装）での方法
    Widget example() {
      bool isFirstBuilder = true; // このFlagで非同処理完了していないのに、snapshotにデータが存在しても1回目は非同期処理完了前の処理を通す
      return FutureBuilder(
        future: SSO().getSsoToken(), // 非同期処理
        builder: (context, snapshot) {
          if (snapshot.hasData && isFirstBuilder == false) {
              // 非同期処理完了後
              return Text("")
          } else {
            isFirstBuilder = false;
            // 非同期処理完了前
            return Center(child: CircularProgressIndicator());
          }
        },
      );
    }
    ```