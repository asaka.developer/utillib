# Firebase_Analyticsの使用方法

## **1.Pluginの導入**

以下のPluginを使用し、アプリからFirebase_Analyticsに送信する。  
https://pub.dev/packages/firebase_analytics  

※事前にFirebaseとの接続は確立させておくこと!!  
[Firebaseの使用方法(Flutter).md](Firebaseの使用方法(Flutter).md)を参照。

---

## **2.Firebaseのプロジェクトを作成**

https://console.firebase.google.com/

---

## **3.実装方法**

### **3.1.yamlに以下を追記（Versionは最新のものを使用する）**

```yaml
  firebase_analytics: ^6.0.0
```

### **3.2.FirebaseAnalyticsと通信する**

```dart
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';

class MyApp extends StatelessWidget {
  final FirebaseAnalytics _analytics = new FirebaseAnalytics();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Example",
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      // ここにFirebaseAnaliticsのオブサーバーをセットする
      navigatorObservers:<NavigatorObserver> [
        FirebaseAnalyticsObserver(analytics: _analytics)
      ],
      body: MyHomePage(_analytics);
    );
  }
}

class MyHomePage extends StatefulWidget {
  final FirebaseAnalytics _analytics;
  MyHomePage(this._analytics, {Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  void initState() {
    super.initState();
    // ここで、FirebaseAnalytics周りの処理を呼び出し、Analyticsデータを送信する。
    analyticsTest();
  }

  @override
  Widget build(BuildContext context) {
    return Text("");
  }

  // FirebaseAnalyticsのテスト
  Future<void> analyticsTest() async {
    await widget._analytics.setUserId("TestId");
    await widget._analytics.logEvent(name: "TestEvent", parameters: <String, dynamic>{"Event" : "Example"});
    await widget._analytics.setCurrentScreen(screenName: "ExampleScreen", screenClassOverride: "example");
  }
}
```

---

## **4.確認方法**

- 1.アプリを実行する
- 2.作成したプロジェクトを選択する  
    https://console.firebase.google.com/
- 3.メニューから「Dashboard」を選択する
- 4.「Googleアナリティクスでデータを確認する」を選択する  
    https://analytics.google.com/analytics/web/
- 5.メニューから「DebugView」を選択する

※解析データが反映されていな場合、以下のコマンドを実行してから確認する
```adb
adb shell setprop debug.firebase.analytics.app [PackageName]
```