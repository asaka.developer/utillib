# Firebaseの実装(Flutter)

※ 前段として、[Firebaseの使用方法(Android)](Firebaseの使用方法(Android).md)を要参照。

## **実装方法**

### **参考資料**

- [公式サイト](https://firebase.google.com/docs/flutter/setup?hl=ja)
- [pub.dev](https://pub.dev/packages/firebase_messaging#-readme-tab-)

### **手順**

- 1.[Firebase上のプロジェクトを作成](Firebaseの使用方法(Android).md)を参考に、  
    Android, iOSのプロジェクトを作成する。
    ※ パッケージ名はFCM, Android, iOS共に一致する必要がある。

- 2.Android, iOSのプロジェクト作成で自動生成された`google-services.json`、`GoogleService-Info.plist`を
    以下のディレクトリに格納する。
    - Android: `android/app`に`google-services.json`を配置する。
    - iOS: プロジェクトファイルから[Xcode](https://yaba-blog.com/flutter-first-throw-call-stack/)を起動し、「Runner」→「右クリック」→「Add Files to Runner」をクリックして、`GoogleService-Info.plist`を追加する。  
<img src="image/01_FCMの設定ファイルを追加.png">

- 3.`android/app/build.gradle`に「apply plugin: 'com.google.gms.google-services'」を追加する。
    ```xml
    apply plugin: 'com.android.application'
    apply plugin: 'kotlin-android'
    apply from: "$flutterRoot/packages/flutter_tools/gradle/flutter.gradle"
    apply plugin: 'com.google.gms.google-services' <!-- これを追加 -->

    android {
        compileSdkVersion 28
    ```

- 4.`android/build.gradle`に「classpath 'com.google.gms:google-services:4.3.3'」を追加する。  
    また、Versionは最新しないとBiuldが通らないことがある。
    ```xml
    buildscript {
        ext.kotlin_version = '1.3.50'
        repositories {
            google()
            jcenter()
        }

        dependencies {
            classpath 'com.android.tools.build:gradle:3.5.0'
            classpath "org.jetbrains.kotlin:kotlin-gradle-plugin:$kotlin_version"
            classpath 'com.google.gms:google-services:4.3.3' <!-- これを追加 -->
        }
    }
    ```

- 5.`pubspec.yaml`に「firebase_messaging: ^6.0.16」を追加する。  
    また、Versionは最新しないとBiuldが通らないことがある。
    ```yaml
    dependencies:
        flutter:
            sdk: flutter
        firebase_messaging: ^6.0.16 # これを追加
    ```

- 6.Terminalで`flutter pub get`を実行する。

- 7.main.dart(ルートファイル)のStatefulWidgetのStateで、FCMの処理を実装する。
    ```dart
    import 'package:firebase_messaging/firebase_messaging.dart';

    class _MyHomePageState extends State<MyHomePage> {
      final FirebaseMessaging _firebaseMessaging = new FirebaseMessaging();
      @override
      void initState() {
        super.initState();
        _firebaseMessaging.configure(
          onMessage: (Map<String, dynamic> message) async {
            print("onMessage: $message");
          },
          onLaunch: (Map<String, dynamic> message) async {
            print("onLaunch: $message");
          },
          onResume: (Map<String, dynamic> message) async {
            print("onResume: $message");
          },
        );
        _firebaseMessaging.requestNotificationPermissions(
            const IosNotificationSettings(sound: true, badge: true, alert: true));
        _firebaseMessaging.onIosSettingsRegistered
            .listen((IosNotificationSettings settings) {
          print("Settings registered: $settings");
        });
        _firebaseMessaging.getToken().then((String token) {
          assert(token != null);
          print("Push Messaging token: $token");
        });
        _firebaseMessaging.subscribeToTopic("/topics/all");
      }
    }
    ```
- 8.Terminalで`flutter run`を実行する。
- 9.FirebaseのFCMでキャンペーンをAndroid, iOS別に作成する。  
    ターゲットはAndroid, iOSのプロジェクトを選択する。
    キャンペーン作成の表示は[こちら](https://console.firebase.google.com/u/0/)から対象プロジェクトを選択し、  
    FCMの画面に移動して作成する。