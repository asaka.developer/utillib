import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:encrypt/encrypt.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UtilEncrypt {

  // 暗号化してからプリファレンスに保存する
  void savePreference(String key, String value) async {
    print("save preference");
    if (value == null) {
      // 値が存在しないため、プリファレンスには保存しない
      return;
    }
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String data = await _encrypt(value);
    prefs.setString(key, data);
  }

  // プリファレンスから読み込み復号化する
  Future<String> loadPreference(String key) async {
    print("load preference");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String data = prefs.getString(key);
    return _decrypt(data);
  }

  void clearPreference() async {
    print("remove preference");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();
  }

  // プリファレンスに保存したい値を暗号化する
  Future<String> _encrypt(String data) async {
    String deviceId = await _getDevicesInfo();
    // デバイス毎のユニークなIDで暗号化する
    print("ask_log deviceId: $deviceId");
    print("ask_log data: $data");
    final key = Key.fromUtf8(deviceId);
    final iv = IV.fromLength(16);

    final encrypter = Encrypter(AES(key));
    try {
      final encrypted = encrypter.encrypt(data, iv: iv);
      return encrypted.base64;
    } catch (e) {
      print("暗号化に失敗しました");
      print(e);
      return null;
    }
  }

  // 暗号化されたデータを復号化する
  Future<String> _decrypt(String encryptedData) async {
    String deviceId = await _getDevicesInfo();
    // デバイス毎のユニークなIDで復号化する
    final key = Key.fromUtf8(deviceId);
    final iv = IV.fromLength(16);

    final encrypter = Encrypter(AES(key));
    try {
      final decryptedData = encrypter.decrypt64(encryptedData, iv: iv);
      return decryptedData;
    } catch (e) {
      print("復号化に失敗しました");
      print(e);
      return TokenPreferenceError.failedDecrypt;
    }
  }

  // 端末固有番号を取得する（ユニーク）
  Future<String> _getDevicesInfo() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    if (Platform.isAndroid) {
      AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
      // AndroidのデバイスIDを出力
      return androidDeviceInfo.androidId;
    } else {
      IosDeviceInfo iosDeviceInfo = await deviceInfo.iosInfo;
      // iOSのUUIDｗ出力
      return iosDeviceInfo.identifierForVendor;
    }
  }
}