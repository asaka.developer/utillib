# OSの判定について

```dart
import 'dart:io';

// OS名を取得、自分でごりごりやりたいならこちら
String os = Platform.operatingSystem;

// OSごとで判定するプロパティも提供されている
bool isAndroid = Platform.isAndroid;
```