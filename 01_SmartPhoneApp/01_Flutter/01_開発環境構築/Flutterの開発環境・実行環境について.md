# Flutterの開発環境・実行環境について

## 1.インストール

https://flutter.dev/docs/get-started/install からインストールする。  
インストール手順は上記URLでクリックしたOSの手順に沿って行う。

- 1.Terminalで`git clone https://github.com/flutter/flutter.git -b stable`を実行する  
    Gitが導入されていない場合は以下からFlutterのSDKを直接ダウンロードする
    - Windowsの場合: https://flutter.dev/docs/get-started/install/windows
    - MacOSの場合: https://flutter.dev/docs/get-started/install/macos
- 2.Flutter SDKの環境変数を通す
- 3.Terminalで`flutter --version`を実行し、FlutterのSDKがインストールされていることを確認する

---

## **2.Flutterの依存関係を確認**

Flutterのインストール後、`flutter doctor`のコマンドを実行し、Flutterの依存関係を確認する。

```terminal
// このコマンドを実行する
flutter doctor

Doctor summary (to see all details, run flutter doctor -v):
[√] Flutter (Channel stable, v1.17.5, on Microsoft Windows [Version 10.0.18362.900], locale ja-JP)

[!] Android toolchain - develop for Android devices (Android SDK version 30.0.1)
    ! Some Android licenses not accepted.  To resolve this, run: flutter doctor --android-licenses
[√] Android Studio (version 4.0)
[√] VS Code
[√] Connected device (1 available)
```

### **2.1.Android_Studioで警告マークが付く**

Android Studioのライセンスに同意できていないため発生する。
以下のコマンドを実行し、ライセンスに同意する。

```terminal
// このコマンドを実行する
flutter doctor --android-licenses

// ここにライセンスが表示される。
// Acceptの所でyesを実行し承諾する。

---------------------------------------
Accept? (y/N): y
```

---

## **3.Flutterの開発チャンネルを確認する**

- 1.Terminalで`flutter channel`を実行し、`stable`になっている事を確認する
    - `stable`になっていない場合は`flutter channel stable`を実行し、再度`手順1`を行う

### **補足**

以下のChannelが存在する

- master（最新の機能を試せるが、不安定）
- dev（masterをテストしたチャンネル、不安定）
- beta（devの安定版）
- stable（安定版: `開発時はこれを使用すること`）

---

## **4.Android StudioでFlutterを使用できるようにする**

### **4.1.Android StudioにFlutterのPluginを導入する**

- 1.Android Studioを起動する  
<img src="image/4.1.1.png" width="350">

- 2.Pluginの一覧を開く  
<img src="image/4.1.2.png" width="350">

- 3.FlutterのPluginをインストールする  
  （画像はインストール後のものです）  
<img src="image/4.1.3.png" width="350">

### **4.2.Flutterのプロジェクトを作成する**

- 1.Start new Flutter projectを選択する  
<img src="image/4.2.1.png" width="350">

- 2.Flutter Applicationを選択する  
<img src="image/4.2.2.png" width="350">

- 3.作成するプロジェクトの詳細を入力する  
    入力後は「Next」をクリックする
    - Project name: プロジェクトの名前
    - Flutter SDK path: [1.インストール](#1.インストール)で保存したSDKのパス
    - Project location: プロジェクトの保存場所のパス
    - Description: プロジェクトの概要

    <img src="image/4.2.3.png" width="350">

- 4.Package nameに任意のパッケージ名を入力する
    入力後は「Finith」をタップする

    <img src="image/4.2.4.png" width="350">

- 5.アプリの作成完了を待つ  
<img src="image/4.2.5.png" width="350">

- 6.アプリ完成後は以下の画面が表示される  
<img src="image/4.2.6.png" width="350">

### **4.3.フォルダ構成について**

- android: AndroidのNative処理を実装する際に使用する
- ios: iOSのNative処理を実装する際に使用する
- lib: Flutterの処理を実装する際に使用する
- pubspec.yaml: Flutterで使用するライブラリやアセットを設定する際に使用する  
<img src="image/4.3.png" width="350">
