# Blocについて

## **概要**

BlocはUIとBusiness Logicを分けるために使用する。  
<img src="image/bloc_architecture1.png" width="200">  
<img src="image/bloc_architecture2.png" width="415">  
<img src="image/call_data_flow.png" width="415">  

- 詳細は[https://bloclibrary.dev/#/jp/](https://bloclibrary.dev/#/jp/)を参照

---

## **自分なりのまとめ**

### **実装方法（UI + Bloc）**

- 1.Blocを作成する
    - Blocクラスを継承する
    - Blockには`Event`を渡す
    - Eventは自作クラスでも問題ない
    - 渡したEvent毎に処理を分岐し、Eventに応じた処理を行う
    - 完了した処理の値(State)は`yield`でBlocBuilderに伝達する（Stream）

- 2.WidgetにBlocProviderを実装する
    - BlocProviderで、Blocの作成処理を実装する
    - また、子Widgetを作成する

- 3.BlocProviderの子WidgetでBlockBuilderを作成する
    - BlocBuilder内では、Blocが完了して返ってくる`State`をWidgetに反映させる処理を実装する

- 4.Widgetのボタンなど、何かしらのトリガー時に、BlocにEventを追加する処理を実装する
    - BlocにEventを追加すると、Bloc側の処理が走り、最終的にBlocBuilder内に`State`が返ってくる

### **実装方法（UI + Bloc + Repository + DataProvider）**

- 1.Event(独自クラス)を作成する
    - Equatableを継承し、Event(独自クラス)を作成する
    - Event(独自クラス)を作成すると、Blocに独自インスタンスを渡す事となるので、複数のデータを内包し渡すことができる
    - `Blocに複数の値を渡す必要がない場合、Eventは文字列でこと足りるので、Event(独自クラス)の作成は不要`

- 2.State(独自クラス)を作成する
    - Equatableを継承し、Stateを作成する
    - State(独自クラス)を作成すると、BlocからBlocBuilderに独自インスタンスを渡す事になるので、複数のデータを内包し渡すことができる
    - `BlocBuilderに複数の値を渡す必要がない場合、Eventはプリミティブ型でこと足りるので、State(独自クラス)の作成は不要`

- 3.DataProviderを作成する
    - Repositoryから呼び出される処理を実装する（非同期処理）
    - 非同期処理が完了すると、Repositoryに結果が渡される（Future）

- 4.Repositoryを作成する
    - Blocから呼び出すためのRepositoryを作成する
    - RepositoryはDataProviderの統括役である（非同期処理の集合体）
    - Repositoryでは複数のDataProviderを実行する
    - Repositoryの実行結果はBlocに伝達する（Future）

- 5.Blocを作成する
    - Blocクラスを継承する
    - Blockには`Event`を渡す
    - Eventは自作クラスでも問題ない
    - 渡したEvent毎に処理を分岐し、Eventに応じた処理を行う
    - ここでは`Repositoryの処理`を呼び出す
    - 完了した処理の値(State)は`yield`でBlocBuilderに伝達する（Stream）

- 6.WidgetにBlocProviderを実装する
    - BlocProviderで、Blocの作成処理を実装する
    - また、子Widgetを作成する

- 7.BlocProviderの子WidgetでBlockBuilderを作成する
    - BlocBuilder内では、Blocが完了して返ってくる`State`をWidgetに反映させる処理を実装する

- 8.Widgetのボタンなど、何かしらのトリガー時に、BlocにEventを追加する処理を実装する
    - BlocにEventを追加すると、Bloc側の処理が走り、最終的にBlocBuilder内に`State`が返ってくる
