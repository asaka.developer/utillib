import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:testbloc/test_provider.dart';

enum CounterEvent { increment, decrement }

// こちらがBlocの実装例

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    print("_MyHomePageState build");
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: BlocProvider<CounterBloc> (
          create: (context) => CounterBloc(),
          child: CounterPage()
        )
        // body: TestCounter());
  }
}

class CounterBloc extends Bloc<CounterEvent, int> {
  @override
  int get initialState => 0;

  @override
  Stream<int> mapEventToState(CounterEvent event) async* {
    switch (event) {
      case CounterEvent.increment:
        yield state + 100;
        break;
      case CounterEvent.decrement:
        yield state - 1;
        break;
    }
  }
}

class CounterPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print("CounterPage build");
    final CounterBloc counterBlock = BlocProvider.of<CounterBloc>(context);
    return Scaffold(
        appBar: AppBar(title: Text("Counter")),
        body: BlocBuilder<CounterBloc, int>(builder: (context, count) {
          print("bloc builder");
          return Center(
              child: Text("$count", style: TextStyle(fontSize: 24.0)));
        }),
        floatingActionButton: Column(
          children: <Widget>[
            FloatingActionButton(
              child: Icon(Icons.add),
              onPressed: () {
                counterBlock.add(CounterEvent.increment);
              },
            ),
            FloatingActionButton(
              child: Icon(Icons.remove),
              onPressed: () {
                counterBlock.add(CounterEvent.decrement);
              },
            )
          ],
        ));
  }
}
