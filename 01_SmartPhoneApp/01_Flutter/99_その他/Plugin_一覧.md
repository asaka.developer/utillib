# Plugin_一覧

## **WebView**

- [WebView](https://pub.dev/packages/webview_flutter) → バグが多すぎて使えない
- [WebviewScaffold](https://pub.dev/packages/flutter_webview_plugin) → 一番安定している
- [inappwebview](https://pub.dev/packages/flutter_inappwebview) → キーボードに不具合あり

## **ファイル関連**

- [ファイルへのアクセス](https://pub.dev/packages/path_provider)
- [端末内から画像を選択する](https://pub.dev/packages/image_picker)
- [端末内からファイルを選択する](https://pub.dev/packages/file_picker)

## **動画関連**

- [動画を再生](https://pub.dev/packages/video_player#-installing-tab-)
- [動画からサムネイルを生成](https://pub.dev/packages/video_thumbnail)
- [動画のアスペクト比、再生方法](https://pub.dev/packages/chewie#-readme-tab-)

## 端末

- [端末の情報](https://pub.dev/packages/device_info)

## 保存関係

- [暗号化、復号化](https://pub.dev/packages/encrypt)
- [プリファレンス](https://pub.dev/packages/shared_preferences)
- [SQLite](https://pub.dev/packages/sqflite)

# 他のアプリを起動する

- [URLから他のアプリを起動](https://pub.dev/packages/url_launcher)