# Nativeとの連携方法（Android）

## 概要

Flutter(Dart)から、AndroidのNative処理を呼び出す方法について記述する。  

[MethodChannelの実装方法](https://qiita.com/kurun_pan/items/db6c8fa94bbfb5c0c8d7)  
[Kotlinに適した実装の参考](https://qiita.com/mkosuke/items/b384035e507ad0208c10)  

- 参考資料通りに実装してもBuildが通らない
    - import文が不足しているとBuildは通らない
    - 親クラスを参考にimport文を追加する必要がある
    - ホットリロードはせずに必ずBuildを行うこと

---

## 実装方法

### **1.Dart側**

- 1.MethodChannelを作成する
    - `Nativeと連携するためのCHANNEL`
    - CHANNEL名は自由だが、`パッケージ名/Prefix`が一般的である

- 2.Nativeに渡すパラメータを作成する
    - パラメータは`Map`で作成（final Map params = <String, dynamic>）

- 3.Nativeの処理を実装する
    - Nativeの処理は`非同期`で行う
    - 必要に応じて戻り値を受け取る
    - Nativeには`メソッド名`と`パラメータ`を渡す  
      ```dart
      var result = await _channel.invokeMethod('getResult', params)
      ```

- 4.Dart側から`手順3`で作成した処理を呼び出す

**実装例**  

```dart
  // CHANNEL名
  static const MethodChannel _channel =
      const MethodChannel("com.example.test_native/interop");

  static Future<void> callNative() async {
    // ネイティブ側に渡すパラメータ
    final Map parameters = <String, dynamic> {
      "name": "my name is Jiro",
      "age": 25
    };
    // ネイティブ側にメソッド名とパラメータを渡す
    var result = await _channel.invokeMethod("getResult", parameters);
    // ネイティブ側からの戻り値をログに表示
    print("result: $result");
  }
```

### **2.Native側**

※ 自動補完されないため、実装時の入力内容はよく確認すること

- 1.import文を追加する  
    ※ 必要に応じてimport文は追加すること

- 2.`CHANNEL名`と`Method名`を追加する  
    - `CHANNEL名`と`Method名`は、Dart側で実装した命名（文字列）と同じにする  
      ※ 命名（文字列）が異なると、Dart側と連携きない
    ```kotlin
    private const val CHANNEL = "com.example.test_native/interop"
    private const val METHOD_GET_RESULT = "getResult"
    ```

- 3.Native側で行う処理を実装する
    - `configureFlutterEngine`メソッドをoverrideする
    - チャンネルを初期化する
    - Dartから`メソッド名`、`パラメータ`を受け取る
    - Dartから受け取った`メソッド名`が、目的のメソッド名かを確認する
    - Dartから受け取った`パラメータ`を使用し、目的のNativiの処理を行う
    - Nativeの処理結果をDart側に返す

**実装例**  
```kotlin
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodCall
import io.flutter.plugins.GeneratedPluginRegistrant
import android.util.Log

class MainActivity: FlutterActivity() {
    companion object {
        // lib(dart)側と連携するためのCHANNEL名 → CHANNEL名は自由だが、パッケージ名/Prefixが一般的とのこと
        private const val CHANNEL = "com.example.test_native/interop"
        // 定義したいメソッド名
        private const val METHOD_GET_RESULT = "getResult"
    }

    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)

        // この処理は無くても動いたので不要かも
        GeneratedPluginRegistrant.registerWith(flutterEngine)

        // MethodChannelからのメッセージを受け取ります
        // （flutterViewはFlutterActivityのプロパティ、CHANNELはcompanion objectで定義しています）
        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, CHANNEL)
                .setMethodCallHandler { methodCall: MethodCall, result: MethodChannel.Result ->
                    if (methodCall.method == METHOD_GET_RESULT) {
                        val name = methodCall.argument<String>("name").toString()
                        val age = methodCall.argument<Int>("age")
                        Log.d("Android", "name = ${name}, age = $age")
                        result.success("success")
                    } else {
                        result.notImplemented()
                    }
                }
    }
}
```