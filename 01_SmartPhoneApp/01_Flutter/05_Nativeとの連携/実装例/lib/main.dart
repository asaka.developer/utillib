import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  // ネイティブと連携するためのCHANNEL → CHANNEL名は自由だが、パッケージ名/Prefixが一般的とのこと
  static const MethodChannel _channel =
      const MethodChannel("com.example.test_native/interop");

  static Future<void> callNative() async {
    // ネイティブ側に渡すパラメータ
    final Map parameters = <String, dynamic>{
      "name": "my name is Jiro",
      "age": 25
    };
    // ネイティブ側にメソッド名とパラメータを渡す
    String result = await _channel.invokeMethod("getResult", parameters);
    // ネイティブ側からの戻り値をログに表示
    print("result: $result");
  }

  @override
  void initState() {
    super.initState();
    // ネイティブの処理を呼び出す
    callNative();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("test"),
      ),
      body: Center(),
    );
  }
}
