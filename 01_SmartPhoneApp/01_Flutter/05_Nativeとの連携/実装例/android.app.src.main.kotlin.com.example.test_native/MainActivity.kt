package com.example.test_native

// import抜けに注意
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodCall
import io.flutter.plugins.GeneratedPluginRegistrant
import android.util.Log
import android.os.Bundle

class MainActivity: FlutterActivity() {
    companion object {
        // lib(dart)側と連携するためのCHANNEL名 → CHANNEL名は自由だが、パッケージ名/Prefixが一般的とのこと
        private const val CHANNEL = "com.example.test_native/interop"
        // 定義したいメソッド名
        private const val METHOD_GET_RESULT = "getResult"
    }

    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)

        // この処理は無くても動いたので不要かも
        GeneratedPluginRegistrant.registerWith(flutterEngine)

        // MethodChannelからのメッセージを受け取ります
        // （flutterViewはFlutterActivityのプロパティ、CHANNELはcompanion objectで定義しています）
        MethodChannel(flutterEngine.dartExecutor.binaryMessenger, CHANNEL)
                .setMethodCallHandler { methodCall: MethodCall, result: MethodChannel.Result ->
                    if (methodCall.method == METHOD_GET_RESULT) {
                        val name = methodCall.argument<String>("name").toString()
                        val age = methodCall.argument<Int>("age")
                        Log.d("Android", "name = ${name}, age = $age")
                        result.success("success")
                    } else {
                        result.notImplemented()
                    }
                }
    }
}