import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class HistoryDao {

  final String DB_NAME = "History.db";
  final String TABLE_NAME = "history";
  final String _NAME = "name";
  final String _URL = "url";
  final String _DATE = "date";

  // DBを開く
  Future<Database> openDB() async {
    print("openDB");
    return openDatabase(
        join(await getDatabasesPath(), DB_NAME),
        onCreate: (db, version) {
          // テーブルを作成する
          db.execute("CREATE TABLE $tableName($_NAME TEXT, $_URL TEXT, $_DATE TEXT)");
        },
        version: 1
    );
  }

  // 履歴を挿入する
  void insertHistory(Database db, HistoryModel historyModel) {
    Map<String, String> map = {
      _NAME : HistoryModel.name,
      _URL : HistoryModel.url,
      _DATE : HistoryModel.date
    };
    db.insert(TABLE_NAME, map);
  }

  // 履歴を更新する
  void updateHistory(Database db, HistoryModel historyModel) {
    Map<String, String> map = {
      _NAME : HistoryModel.name,
      _URL : HistoryModel.url,
      _DATE : HistoryModel.date
    };
    db.update(TABLE_NAME, map, where: "$_NAME = ?", whereArgs: ["test"]);
  }

  // 履歴を削除する
  void deleteHistory(Database db, HistoryModel HistoryModel) {
    db.delete(TABLE_NAME, where: "$_NAME = ?", whereArgs: [HistoryModel.name]);
  }
}