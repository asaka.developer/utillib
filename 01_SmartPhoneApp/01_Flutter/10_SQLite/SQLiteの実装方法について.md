# SQLiteの実装方法について

## **1.概要**

FlutterでのSQLiteの実装方法について記述する。

※ 作成したDBはPC上でSQLを確認する必要がある。  
   `db`, `db-shm`, `wal`の3ファイルをアプリ内領域から取得しないとPCでは確認できない。

---

## **2.使用Plugin**

- [SQLiteを使用](https://pub.dev/packages/sqflite)
- [DBの格納場所を取得](https://pub.dev/packages/path)

---

## **3.実装方法**

### **3.1.プラグインの読み込み**

`pubspec.yaml`に以下を読み込ませる

- sqflite: $version
- path: $version

※$versionは[2.使用Plugin](#2.使用Plugin)を参照。

### **3.2.DBの作成、及びテーブルの作成**

```dart
// DBの作成
openDatabase(
    join(await getDatabasesPath(), "DB_NAME"),
    onCreate: (db, version) {
      // テーブルの作成
      db.execute("CREATE TABLE $tableName($_NAME TEXT, $_URL TEXT, $_DATE TEXT)");
    },
    version: 1
);
```

### **3.3.挿入（INSERT）**

```dart
// 挿入する
void insert(Database db, Model model) {
  print("insertHistory");
  Map<String, String> map = {
    "_NAME" : model.name,
    "_URL" : model.url,
    "_DATE" : model.date
  };
  // 挿入処理
  db.insert(TABLE_NAME, map);
}
```

### **3.4.更新（UPDATE）**

```dart
// 更新する
void update(Database db, Model model) {
  print("updateHistory");
  Map<String, String> map = {
    "_NAME" : model.name,
    "_URL" : model.url,
    "_DATE" : model.date
  };
  // 更新処理（["args"]に条件値を指定する）
  db.update(TABLE_NAME, map, where: "_NAME = ?", whereArgs: ["args"]);
}
```

### **3.5.削除（DELETE）**

```dart
// 削除する
void delete(Database db) {
  // 削除処理（["args"]に条件値を指定する）
  db.delete(TABLE_NAME, where: "_NAME = ?", whereArgs: ["args"]);
}
```
