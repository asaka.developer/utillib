# Providerについて

## **概要**

- 特定のWidgetのみ再Buildすることができる。
- 子孫のWidgetにデータ（値）を共有できる。
- ProviderでMVVM開発をすると、UIとビジネスロジックを分離できる。

### **参考資料**

- [MVVMの説明](https://note.com/yasukotelin/n/n3cdd311fb336)
- [MVVMの実装例](https://github.com/yasukotelin/mvvm_example)
- [Providerについてのデベロッパーサイト](https://pub.dev/packages/provider)

---

## **アーキテクチャ**

<img src="image/mvvm_architecture.png" width="800"/>

---

## **Blocとの違い**

`ProviderのMVVM開発`と`Bloc`はよく似ている。  
`ProviderのMVVM開発`は後発、`Bloc`は先発で公開された。  
以前は`Bloc`をGoogleが推奨していたが、大がかりな実装となってしまう。  
ここに疑問を覚えたGoogleは`ProviderのMVVM開発`を推奨するようになった。  
しかしながら、`Bloc`の良さもあるので、プロジェクトの規模や内容によって使い分けると良い。
`Bloc`については[こちら](../Bloc/Blocについて.md)を参照。  

---

## **自分なりのまとめ**

### **pubspec.yamlの実装方法**

以下を追記する

```yaml
provider: ^4.1.2
```

### **MVVMの実装方法**

- 1.ChangeNotifierの継承クラスを作成する  
    ※ `このクラスがViewModel`。
    - 非同期処理を実装する
    - 非同期処理が完了したら、Widgetに通知する
      ※ `notifyListeners()`

- 2.WidgetからChangeNotifierの継承クラスを呼び出す
    - ChangeNotifierProviderを実装する（複数ある場合はMultiProviderを使用する）
    - ChangeNotifierProviderの子に、非同期前・非同期後のWidgetを実装する。
        - 1.Providerから、上記実装のChangeNotifier(ViewModel)を呼び出す
        - 2.ViewModelから、表示したいデータ（値）を取得し画面に反映する
           （非同期処理後に、特定のWidgetのみBuildしたい場合はConsumer配下に実装する）  
            (特定のWidgetを指定しない場合はConsumerの作成は不要)

- 3.Widgetにボタンなどのトリガーを実装する
    - トリガー時に、Providerから上記実装のChangeNotifier(ViewModel)を呼び出す。
    -  ViewModelから非同期で処理を行いたいメソッドを呼び出す。
