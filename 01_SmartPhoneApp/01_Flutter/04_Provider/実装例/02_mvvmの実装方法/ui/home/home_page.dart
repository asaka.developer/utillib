import 'package:flutter/material.dart';
import 'package:mvvm_example/ui/home/home_view_model.dart';
import 'package:provider/provider.dart';

import 'home_view_model.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print("HomePage build");    
    // 複数のProviderを実装する予定があるのならこちら
    return MultiProvider(
      providers: [
        // Injects HomeViewModel into this widgets.
        ChangeNotifierProvider(create: (_) => HomeViewModel()),
      ],
      child: Scaffold(
        appBar: AppBar(
          title: Text("Home"),
        ),
        body: HomePageBody(),
        floatingActionButton: _HomePageFloatingActionButton(),
      ),
    );
    // 1つのProviderのみを実装するのならこちら
    // return ChangeNotifierProvider(
    //   create: (
    //     context) => HomeViewModel(),
    //   child: Scaffold(
    //     appBar: AppBar(
    //       title: Text("Home"),
    //     ),
    //     body: HomePageBody(),
    //     floatingActionButton: _HomePageFloatingActionButton(),
    //   ),
    // );
  }
}

class _HomePageFloatingActionButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print("_HomePageFloatingActionButton");
    return FloatingActionButton(
      onPressed:
          Provider.of<HomeViewModel>(context, listen: false).incrementCounter,
      tooltip: 'Increment',
      child: Icon(Icons.add),
    );
  }
}

class HomePageBody extends StatefulWidget {
  @override
  _HomePageBodyState createState() => _HomePageBodyState();
}

class _HomePageBodyState extends State<HomePageBody> {
  @override
  Widget build(BuildContext context) {
    print("HomePageBody build");
    // Consumer配下のみBuildする
    return Consumer<HomeViewModel>(
      builder: (context, value, child) {
        return Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'You have pushed the button this many timesss:',
              ),
              Text(
                Provider.of<HomeViewModel>(context,listen: false).counter.toString(),
                style: Theme.of(context).textTheme.display1,
              ),
            ],
          ),
        );
    },);
    // _HomePageBodyState配下をBuildする
    // return Center(
    //   child: Column(
    //     mainAxisAlignment: MainAxisAlignment.center,
    //     children: <Widget>[
    //       Text(
    //         'You have pushed the button this many times:',
    //       ),
    //       Text(
    //         Provider.of<HomeViewModel>(context).counter.toString(),
    //         style: Theme.of(context).textTheme.display1,
    //       ),
    //     ],
    //   ),
    // );
  }
}
