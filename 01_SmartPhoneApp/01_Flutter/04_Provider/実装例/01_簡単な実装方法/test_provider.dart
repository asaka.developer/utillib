import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

// こちらがProvider実装例

class TestCounter extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print("TestCounter");
    return ChangeNotifierProvider(
      create: (context) => TestChangeNotifier(),
      child: MyPage(),
    );
  }
}

class MyPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    print("MyPage");
    final testChangeNotifier = Provider.of<TestChangeNotifier>(context);
    return Scaffold(
      body: Column(
        children: <Widget>[
          Text("${testChangeNotifier.count1}"),
          Text("${testChangeNotifier.count2}")
        ],
      ),
      floatingActionButton: Row(
        children: <Widget>[
          FloatingActionButton(
            // onPressed: testChangeNotifier.increment(),
            onPressed: () => {testChangeNotifier.increment()},
            child: Icon(Icons.add),
          ),
          FloatingActionButton(
              onPressed: () => {testChangeNotifier.decrement()},
              child: Icon(Icons.remove))
        ],
      ),
    );
  }
}

class TestChangeNotifier extends ChangeNotifier {
  int count1 = 1;
  int count2 = 2;

  increment() {
    count1++;
    count2++;
    notifyListeners();
  }

  decrement() {
    count1--;
    count2--;
    notifyListeners();
  }
}
