# BindingAdapterについて

BindingAdapterは、レイアウトの表示周りを担当します。  
BindingAdapterを作成し、xmlからBindingAdapterを使用することでレイアウトの更新が可能です。

## **独自Attributeの作成**

BindingAdapterを使用すると以下が実現できます。
- xml
    - 独自の属性を定義できる
    - 独自の属性に値をセットできる

- View
    - xmlで定義した属性に対する処理を実装できる
    - 以下は例
        - 1.xmlで独自属性を作成
        - 2.独自属性の値に応じて、LayoutParamを変更する処理を実装する
        - 3.xmlで独自属性に値をセットする
        - 4.BindingAdapterを使用して、xmlにセットされた値に応じた処理（この場合だとLayoutParam）の変更を実行する
        - 上記により、ActivityやFragmentの巨大化を防止する
        - `BindingAdapterはView → ViewModelのDataBindingである`

```xml
homeViewModelはViewModelのインスタンスです。   
※BindingAdapterはstaticメソッドにする必用があります。

<TextView
    android:id="@+id/highlight_web_view"
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
    android:visibility="invisible"
    scoreParam="@{homeViewModel.context}"/>
```

```kotlin
companion object {
    @BindingAdapter("scoreParam")
    @JvmStatic fun scoreParam(view: View, context: Context) {
        // ここでTextView初期化時のscoreParamで行いたい処理を実装する
        // LayoutParamsの調整など
        // (下記のxmlが例)
    }
}
```

---

## **既存のクラスを拡張**

独自のAttributeの追加ではなく、既存のクラスにメソッドを追加することが可能です。  
メソッドを追加するとxmlから呼び出せます。  
※BindingAdapterはstaticメソッドにする必用があります。

```xml
homeViewModelはViewModelのインスタンスです
<TextView
    android:id="@+id/highlight_web_view"
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
    android:visibility="invisible"
    app:loadScore=""/>
```

```kotlin
companion object {
    @BindingAdapter("loadScore")
    @JvmStatic fun TextView.loadScore(param: String) {
        // これでTextViewにloadScoreのメソッドを拡張した事になる
        // TextViewのloadScoreメソッドで行いたい処理を実装する
        // paramは実装例として記述しているだけですので、必用に応じて型をご変更下さい。
    }
}
```

---

## **BindingAdapterの動き**

BindingAdapterを使用すると、最初のレイアウト初期化時に、BindingAdapterのデータが間に合わないケースもあります。  
<br>
例）非同期処理の結果をViewModelに反映し表示させたいが、非同期処理の完了前にレイアウトの初期化が行われる。  
    上記の場合、DataBindingにViewModelのLiveDataを渡しておけば、`ViewModelのLiveData更新時に、再度BindingAdapterの処理が実施されます。`    

- `LiveData、またはBindingAdapterに反映`させないと、際実施されないので要注意（BindingMethodsは駄目！！）  
- `DataBindingにViewModelを渡さ`ないと際実施されないので要注意！！  

---

## **BindingMethod**

xmlからViewModelのメソッドなどを呼び出す際に使用します。  
明記していない場合は暗黙的にBindingMethod扱いとなります。  
そのため、何も意識せず使用している場合はBindingMethodです。

---

## **@Bindable**

@Bindableを使用すると、トグルボタンのように、ボタンの切り替えが実現できます。