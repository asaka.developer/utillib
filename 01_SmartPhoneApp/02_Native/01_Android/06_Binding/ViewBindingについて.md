# ViewBinderについて

## **1.概要**

ViewBinderとは、プログラム形式ではなく宣言形式を利用してLayout内のViewをアプリソースにバインドする事です。 
従来の方法であれば、Layout内からViewを参照する再は`findViewById`を使用し、Viewを参照します。   
しかし、この方法だたViewの参照に失敗してnullが返却されるリスクがあります。

ViewBinderを使用すると直接Viewを参照するため、nullが返ってくる事はありません。
- https://developer.android.com/topic/libraries/view-binding?hl=ja
- https://qiita.com/hohohoris/items/cdfdc30f435be1c3c558

---

## **2.実装方法**

### **2.1.appのgradle**

Appモジュールのgradleに以下を追加する必要がある

```gradle
android {
    viewBinding {
        enabled = true
    }
}
```

---

### **2.2.ソースコード**

Activityは`activity_main.xml`を使用すると仮定します。  
  
`activity_main.xml`に`{package名}.ActivityMainBinding`をimportします。  
※ Bindする対象は`layout名+Binding`で使用します。

以下の方法で使用できます。  
（`kotlin-android-extensions`と使い方は似ており、`binding変数.{id}`で使用できます）

`以下は実装例です。`

```kotlin
package com.example.testviewbiding

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.testviewbiding.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_main.view.*

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        view.text_view.text = "これはテストです"
        setContentView(view)
    }
}
```

---

### **2.3.レイアウト**

特別に行う事はありません。  
従来通りのxmlと同じです。    
今回はソースコードからTextViewを参照するため、TextViewにidを追加します。

```xml
<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <TextView
        android:id="@+id/text_view"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="Hello World!"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintLeft_toLeftOf="parent"
        app:layout_constraintRight_toRightOf="parent"
        app:layout_constraintTop_toTopOf="parent" />
</androidx.constraintlayout.widget.ConstraintLayout>
```

---

## 3.備考

ViewBinderと似た仕組みで、DataBinderが存在します。
ViewBindingはDataBindingをよりシンプルに扱うために作成されました。  
- https://developer.android.com/topic/libraries/data-binding/expressions?hl=ja

DataBinderについては[こちら](DataBindingについて)のリンクを参照下さい。