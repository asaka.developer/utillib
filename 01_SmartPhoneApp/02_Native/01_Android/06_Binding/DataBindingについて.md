# DataBindingについて

## **1.概要**

DataBindingとは、プログラムではなく宣言形式を使用して、レイアウト内のUIコンポーネントを  
アプリのデータソースにバインドすることです。  
DataBindingライブラリを使用することで実現できます。  
- https://developer.android.com/topic/libraries/data-binding/start?hl=ja
- https://developer.android.com/topic/libraries/data-binding/expressions?hl=ja

<br>

`DataBindingを使用すると、Layoutファイル内に変数を定義する事ができます。`

<br>

Googleのサンプルコードを確認すると、MVP・MVVMで使用されています。  
https://github.com/android/architecture-samples  

※Kotlinでは`kotlin-android-extensions`を使用するとfindViewByIdを省略して記述することが出来ます。  
  しかし、findViewByIdはnullが返ってくる可能性があります。  
  DataBindingはViewに直接参照するため非nullであり、findViewByIdより安全です。  

---

## **2.実装方法**

### **2.1.appのgradle**

Appモジュールのgradleに以下を追加する必要がある。

```gradle
android {
    dataBinding {
        enabled = true
    }
}
```

上記で警告が発生する場合、代わりに以下を追加する。  
(Sync,及びBuildの両方で要確認)

```gradle
android {
    buildFeatures {
        dataBinding = true
    }
}
```

---

### **2.2.ソースコード**

Activityは`activity_main.xml`を使用すると仮定します。  
  
`activity_main.xml`に`{package名}.ActivityMainBinding`をimportします。  
※ Bindする対象は`layout名+Binding`で使用します。
<br>
以下の方法で使用できます。  
（`kotlin-android-extensions`と使い方は似ており、`binding変数.{id}`で使用できます）  
<br>
`以下は実装例です`。  
<br>
※ layoutで使用するデータの変数は、`LiveData`を使用すること！！  
　 通常の変数だと、LiveData更新時に画面上に反映されません！！  
　 BindingAdapterの使用は大丈夫です。

```kotlin
package com.example.testdatabinder

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.example.testdatabinder.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.textViewModel = TextViewModel() //ViewModelを代入する
    }
}
```

```kotlin
package com.example.testdatabinder

data class TextViewModel: ViewModel()(
    val firstName: MutableLiveData<String> by lazy {
        MutableLiveData<String>()
    }
    val lastName: MutableLiveData<String> by lazy {
        MutableLiveData<String>()
    }
)
```

---

### **2.3.レイアウト**

`activity_main.xml`のレイアウトを使用すると仮定します。  
`<layout>`タグを作成し、その中に`<data>`タグと、`<LinearLayout>`などの表示したいレイアウトを記述します。  
`<data>`タグの中には`<Variable>`タグを作成し、そこに変数名とパッケージ+クラス名を記述します。 
```xml
<data>
    <variable name="textViewModel" type="com.example.testdatabinder.TextViewModel"/>
</data>
``` 

```xml
<?xml version="1.0" encoding="utf-8"?>
    <layout xmlns:android="http://schemas.android.com/apk/res/android">
        <data>
            <variable name="textModel" type="com.example.testdatabinder.TextModel"/>
        </data>
       <LinearLayout
           android:orientation="vertical"
           android:layout_width="match_parent"
           android:layout_height="match_parent">
           <TextView android:layout_width="wrap_content"
               android:layout_height="wrap_content"
               android:text="@{textViewModel.firstName}"/>
           <TextView android:layout_width="wrap_content"
               android:layout_height="wrap_content"
               android:text="@{textViewModel.lastName}"/>
       </LinearLayout>
    </layout>
    
```

---

## **3.双方向バインディング**

双方向バインディングを使用すると、レイアウトからソースコードのListenerを呼び出すことができます。

### **3.1.ソースコード**

Activityは`activity_main.xml`を使用すると仮定します。  
そのため、レイアウトから呼び出されるLitenerを追加します。
```kotlin
fun onTextClick() {
    // テキストをクリックした際の処理を実装する
}
```

追加後のソースコードは以下となります。 

```kotlin
package com.example.testdatabinder

data class TextModel(
    val text: String)
{
    fun onTextClick() {
    }
}
```

---

### **3.2.レイアウト**

`activity_main.xml`のレイアウトを使用すると仮定します。  
双方向バインディングにより、レイアウトからソースコードのListenerを呼び出すことが可能です。
そのため、`<TextView>`のタグの中に、以下の要素を追加します。  

```xml
android:onClick="@{()->textModel.onTextClick()}"
```

追加後のxmlは以下となります。

```xml
<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android">

    <data>
        <variable name="textModel" type="com.example.testdatabinder.TextModel"/>
    </data>

    <androidx.constraintlayout.widget.ConstraintLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        tools:context=".MainActivity"
        xmlns:app="http://schemas.android.com/apk/res-auto"
        xmlns:tools="http://schemas.android.com/tools">

        <TextView
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="@{textModel.text}"
            android:onClick="@{()->textModel.onTextClick()}"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintLeft_toLeftOf="parent"
            app:layout_constraintRight_toRightOf="parent"
            app:layout_constraintTop_toTopOf="parent" />
    </androidx.constraintlayout.widget.ConstraintLayout>
</layout>
```

---

### **3.3.ActivityにListenerをセットするパターン**

DataBindingにより、Activityから直接レイアウトを直接参照しているため、  
xmlに`variable`を定義しなくてもxmlからソースコードを呼び出すことが可能です。  
これも双方向バインディングにより実現しています。  
  
以下はレイアウトの一部を抜粋した物です。  

```xml
<TextView
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
    android:text="@{textModel.text}"
    android:onClick="@{onTextClick}"
    app:layout_constraintBottom_toBottomOf="parent"
    app:layout_constraintLeft_toLeftOf="parent"
    app:layout_constraintRight_toRightOf="parent"
    app:layout_constraintTop_toTopOf="parent" />
```
  
以下はソースコードの一部を抜粋した物です。  


```kotlin
fun onTextClick(view: View) {
    // テキストをクリックした際の処理を実装する
}
```

---

## **4.備考**

本ドキュメントではDataBindingについて説明しました。  
DataBindingを使用したMVVMについては以下の記事が分かりやすいです。
- https://qiita.com/hohohoris/items/cdfdc30f435be1c3c558

<br>

また、DataBindingに似た方法で、ViewBindingという物も存在します。  
ViewBindingはDataBindingをよりシンプルに扱うために作成されました。  
- https://developer.android.com/topic/libraries/view-binding?hl=ja  

ViewBinderについては[こちら](ViewBindingについて)のリンクを参照下さい。  
  
ViewModelでContextを使用する際はAndroidViewModelの使用が必用です。