# ViewModelでCoroutineを使用する

## **1.概要**

ViewModelからRepositoryに処理を実行するとき、非同期処理で行う必要があります。  
`viewModelScope`を使用すると、手軽にCoroutineを使用することが出来ます。

---

## **2.AppのGradleで行うこと**

`androidx.lifecycle:lifecycle-viewmodel-ktx`を取得し、`viewModelScope`が使用できるようにします。  

```gradle
//viewmodel-ktx viewModelScopeを使うため
implementation "androidx.lifecycle:lifecycle-viewmodel-ktx:2.2.0"
```

---

## **3.非同期処理の実装方法**

~~ViewModelにて、`viewModelScope.lauch{}`を行うことで、非同期処理を実行できます。~~   
~~非同期処理の結果はLiveDataに反映し、DataBindingによりUIに反映する必要があります。  ~~
`viewModelScope.launch{}は同期処理だった。`

```kotlin
class TestViewModel() : ViewModel() {
    private val mutableLiveData: MutableLiveData<String> = MutableLiveData()

    init {
        viewModelScope.launch {
            // 非同期で行いたい実装
            val result: String = /*ここに非同期処理*/
            // 非同期処理の結果をMutableLiveDataに反映する
                mutableLiveData.value = result
        }
    }
}
```

## **4.備考**

ViewModelでcontextを使用したい場合、`AndroidViewModel`を継承する必用があります。  
AndroidViewModelは、ViewModel参照を含むApplicationのみを拡張する必要があります。
```text
ViewModelsはテストを簡単にする抽象化であるため、テストを簡単にするためにAndroid固有のコードを含めるべきではありません。

ViewModelsにContextのインスタンス、またはContextを保持するViewやその他のオブジェクトのようなものを含めるべきではない理由は、
ActivityおよびFragmentsとは別のライフサイクルを持っているためです。

これが意味することは、アプリで回転の変更を行うとしましょう。

これにより、アクティビティとフラグメントが自身を破壊するため、自身が再作成されます。
ViewModelはこの状態の間持続することを意図しているため、破壊されたアクティビティのビューまたはコンテキストを保持している場合、
クラッシュやその他の例外が発生する可能性があります。

やりたいことをどのように行うべきかについては、MVVMとViewModelはJetPackのDatabindingコンポーネントと非常にうまく機能します。
通常、String、intなどを格納するほとんどの場合、Databindingを使用してビューに直接表示させることができるため、
ViewModel内に値を格納する必要はありません。

しかし、データバインディングが必要ない場合は、コンストラクターまたはメソッド内でコンテキストを渡してリソースにアクセスできます。
 ViewModel内にそのコンテキストのインスタンスを保持しないでください。
```