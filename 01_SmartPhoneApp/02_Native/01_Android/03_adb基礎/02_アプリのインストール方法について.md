# アプリのインストール方法(Android)

## **1.概要**

社内で作成したアプリのインストール手順を記述いたします。  
手順は以下の3パターンの方法があります。  

- [Android_Studioから端末にアプリをインストール](#21Android_Studioから端末にアプリをインストール)
- [パソコンからapkファイルをインストール](#22パソコンからapkファイルをインストール)
- [Android端末内でインストール](#23Android端末内でインストール)

※Android端末をお持ちでない場合、PC上で動作するAndroidエミュレータにインストールすることも可能です。  
 [01_Androidアプリの開発環境・実行環境について.md](01_Androidアプリの開発環境・実行環境について.md)

---

## **2.方法**

※事前に[01_デバッグモード](01_デバッグモード.md)を参考に、デバッグモードを有効化する必用がある。

### **2.1.Android_Studioからインストール**

1. 端末をPCに接続します。（エミュレータでも可）

2. Android Studioで該当プロジェクトを読み込み、アプリの実行ボタンを押すとインストールされます。

### **2.2.パソコンからapkファイルをインストール**

※事前にapkファイルを準備する必要があります。  
　以下は例として準備済のapkファイルを使用したインストール方法です。

1. 端末をPCに接続します。（エミュレータでも可）

2. 以下のadbコマンドでアプリを端末にインストールします。  

3. PC側でインストールするアプリ（apkファイル）が存在するディレクトリに移動します。  
   例）アプリがDesktopに存在する場合。  
  <img src="images/02_app_install/2.2.3.png" width="320">

4. `adb install {apk名}`をコマンドプロンプトで実行し、アプリをインストールします。  
   例）アプリ名が`MyApp`の場合。  
  <img src="images/02_app_install/2.2.4.png" width="320">

  ```
  cd xxx.apkが格納されているディレクトリ}
  ※xxxはインストール対象のapk名に置き換えてください。
  adb install xxx.apk
  例）adb install MyApp.apk
  ```

5. Android端末のホーム画面を開きます。  
<img src="images/02_app_install/2.2.5.png" width="180" height="320"/>

6. インストールしたアプリのアイコンをタップし起動します。  
<img src="images/02_app_install/2.2.6.png" width="180" height="320"/>

<div style="page-break-before:always"></div>

---

### **2.3.Android端末内でインストール**

1. 端末をPCに接続します。（エミュレータでも可）

2. apkファイルをAndroidの任意のディレクトリに保存します。
3. `2`で格納したapkファイルを、Android端末にインストールされている任意のファイルエクスプローラーアプリで開きます。  
<img src="images/02_app_install/2.3.3.png" width="180" height="320"/>

4. 「インストール」ボタンをタップします。  
<img src="images/02_app_install/2.3.4.png" width="180" height="320"/>

<div style="page-break-before:always"></div>

5. Playプロテクトの画面で「OK」をタップします。  
  ※ 開発中のアプリなど、署名が存在しないアプリのインストール時に表示されます。  
<img src="images/02_app_install/2.3.5.png" width="180" height="320"/>

6. 「開く」ボタンをタップし、アプリを起動します。  
<img src="images/02_app_install/2.3.6.png" width="180" height="320">
