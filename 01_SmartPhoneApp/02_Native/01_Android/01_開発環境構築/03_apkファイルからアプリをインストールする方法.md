# apkファイルからアプリをインストールする方法


## 1.概要

社内で作成したアプリのインストール手順を記述いたします。  
手順は以下の2パターンの方法があります。

- [2.1.apkファイルをインストール](#21apkファイルをインストール)
- [2.2.Android端末からインストール](#22Android端末からインストール)

---

## 2.方法

### **2.1.apkファイルをインストール**

- 1.設定アプリを開き、「システム」をタップします。  
<img src="images/03_apk_install/2.1.1.png" width="180" height="320"/>

- 2.「端末情報」をタップします。  
<img src="images/03_apk_install/2.1.2.png" width="180" height="320"/>

- 3.「ビルド番号」を7回連打します。  
  連打後、「これでデベロッパーになりました」と表示されます。  
<img src="images/03_apk_install/2.1.3.png" width="180" height="320"/>

- 4.一つ前の画面に戻り、「詳細設定」をタップします。  
<img src="images/03_apk_install/2.1.4.png" width="180" height="320"/>

- 5.「USBデバッグ」をタップします。  
<img src="images/03_apk_install/2.1.5.png" width="180" height="320"/>

- 6.USBデバッグの許可画面で「OK」をタップします。  
<img src="images/03_apk_install/2.1.6.png" width="180" height="320"/>

- 7.Android端末とPCをUSBケーブルで接続します。

- 8.PC側でインストールするアプリ（apkファイル）が存在するディレクトリに移動します。  
   例）アプリがDesktopに存在する場合。  
<img src="images/03_apk_install/2.1.8.png" width="320">

- 9.`adb install {apk名}`をコマンドプロンプトで実行し、アプリをインストールします。  
   例）アプリ名が`MyApp`の場合。  
<img src="images/03_apk_install/2.1.9.png" width="320">
  ```
  cd xxx.apkが格納されているディレクトリ}
  ※xxxはインストール対象のapk名に置き換えてください。
  adb install xxx.apk
  例）adb install MyApp.apk
  ```

- 10.Android端末のホーム画面を開きます。  
<img src="images/03_apk_install/2.1.10.png" width="180" height="320"/>

- 11.起動したいアプリのアイコンをタップし起動します。  
<img src="images/03_apk_install/2.1.11.png" width="180" height="320"/>

---

### **2.2.Android端末からインストール**

- 1.Android端末とPCをUSBケーブルで接続します。
- 2.apkファイルをAndroidの任意のディレクトリに保存します。
- 3.`2`で格納したapkファイルを、Android端末にインストールされている任意のファイルエクスプローラーアプリで開きます。  
<img src="images/03_apk_install/2.2.3.png" width="180" height="320"/>

- 4.「インストール」ボタンをタップします。  
<img src="images/03_apk_install/2.2.4.png" width="180" height="320"/>

- 5.Playプロテクトの画面で「OK」をタップします。  
  ※ 開発中のアプリなど、署名が存在しないアプリのインストール時に表示されます。  
<img src="images/03_apk_install/2.2.5.png" width="180" height="320"/>

- 6.「開く」ボタンをタップし、アプリを起動します。  
<img src="images/03_apk_install/2.2.6.png" width="180" height="320">