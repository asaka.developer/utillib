# **Androidアプリの開発環境・実行環境について**

## **目次**

- [**1.概要**](#1概要)
- [**2.手順**](#2手順)
  - [**2.1.JDKのインストール**](#21jdkのインストール)
  - [**2.2.Android Studioのインストール**](#22android-studioのインストール)
  - [**2.3.SDKのインストール**](#23sdkのインストール)
  - [**2.4.adbの環境変数を設定**](#24adbの環境変数を設定)
  - [**2.5.Emulatorの作成・実行**](#25Emulatorの作成・実行)

---

## **1.概要**

AndroidのアプリはAndroid Studioを使用し開発を行います。
開発はJDK、及びSDKを使用します。  
また、AndroidのEmulatorはAndroid Studioから実行を行います。  
本資料ではEmulatorでアプリを実行するまでの手順を記載致します。

---

## **2.手順**

### **2.1.JDKのインストール**

JDKをインストール済みの場合はこの手順は省略してください。

1. [JDKのダウンロード、及びインストール](https://www.oracle.com/technetwork/java/javase/downloads/index.html)を実施します。    
   参考資料は[こちら](https://qiita.com/ko2a/items/69fa8a5366d7449500ca)をご参照ください。

   1. JDK Downloadを選択します。  
      <img src="images/01_create_enviroment/2.1.1.1.png" width="350">

   2. PCのOSに応じたファイルを選択します。  
      <img src="images/01_create_enviroment/2.1.1.2.png" width="350">  
      - Windows 64bit の場合: `Windows x64 Installer`のインストーラーファイルをダウンロード
      - macOS 64bit の場合: `macOS Installer`のインストーラーファイルをダウンロード

   3. 規約に同意し、ダウンロードボタンをクリックします。  
      <img src="images/01_create_enviroment/2.1.1.3.png" width="350">  

   4. ダウンロードしたインストーラーファイルを実行します。

   5. 次へをクリックします。  
      <img src="images/01_create_enviroment/2.1.1.5.1.png" width="350">  
    
   6. 宛先フォルダのPathをメモ帳にコピペします。  
      その後、メモ帳にコピペしたPathの後ろに「\bin」を付けます。  
      ※ これが`JDKのPath`です。 

      下記の図だと「`C:\Program Files\Java\jdk-14.0.1\bin`」が環境変数のPathです。  
      Pathのコピーが完了後、「次へ」をクリックします。  
      <img src="images/01_create_enviroment/2.1.1.5.2.png" width="350">  

   7. インストールが完了したら、閉じるをクリックします。  
      <img src="images/01_create_enviroment/2.1.1.6.png" width="350">  

2. ダウンロードしたJDKの環境変数を設定します。  
    参考資料は[こちら](https://web-dev.hatenablog.com/entry/java/jdk/8/windows10-env-variables)をご参照ください。

    1. コントローラーパネルを開きます。
    2. システムとセキュリティを選択します。  
       <img src="images/01_create_enviroment/2.1.2.2.png" width="350">  

    3. システムを選択します。  
       <img src="images/01_create_enviroment/2.1.2.3.png" width="350">  

    4. システムの詳細設定を選択します。  
       <img src="images/01_create_enviroment/2.1.2.4.png" width="350">  

    5. 環境変数を選択します。  
       <img src="images/01_create_enviroment/2.1.2.5.png" width="350">  

    6. システム環境変数の一覧からPathをダブルクリックします。  
       <img src="images/01_create_enviroment/2.1.2.6.png" width="350">  

    7. 「新規」をクリックし、JDKのpathを入力します。  
       <img src="images/01_create_enviroment/2.1.2.7.1.png" width="350">  
       <img src="images/01_create_enviroment/2.1.2.7.2.png" width="350">  

    8. 「OK」をクリックし、環境変数名の編集画面を閉じます。  
       <img src="images/01_create_enviroment/2.1.2.8.png" width="350">  

    9. 「OK」をクリックし、環境変数画面を閉じます。  
       <img src="images/01_create_enviroment/2.1.2.9.png" width="350">  

    10. 「適応」をクリックし、システムのプロパティに反映させます。  
       <img src="images/01_create_enviroment/2.1.2.10.png" width="350">  

---

### **2.2.Android Studioのインストール**

1. [Android Studio](https://developer.android.com/studio?hl=ja)をダウンロードします。  
<img src="images/01_create_enviroment/2.2.1.png" width="350">

2. 規約に同意し、ダウンロードボタンを押します。  
<img src="images/01_create_enviroment/2.2.2.png" width="350">

3. Android Studioをインストールします。  
   参考資料は[こちら](https://qiita.com/ekuzodia_jp/items/887f3fcd542227695387)の「1.Android Studioをインストール」をご参照ください。

    1. Android Studioのインストーラを起動します。    
       デフォルト値のまま「Next」をクリックして進めます。  
       <img src="images/01_create_enviroment/2.2.3.2.1.png" width="350">  
       <img src="images/01_create_enviroment/2.2.3.2.2.png" width="350">  
       <img src="images/01_create_enviroment/2.2.3.2.3.png" width="350">  

    2. 「Choose Start Menu Folder」で「Install」をクリックします。  
       <img src="images/01_create_enviroment/2.2.3.3.png" width="350">  

    3. Completedと表示されたら「Next」をクリックします。  
       <img src="images/01_create_enviroment/2.2.3.4.1.png" width="350">  

    4. インストール完了後、「Start Android Studio」にチェックを入れて「Finish」をクリックします。  
       <img src="images/01_create_enviroment/2.2.3.4.2.png" width="350">  

---

### **2.3.SDKのインストール**

1. Android Studioを起動します。

2. 「Do not import settings」にチェックを入れて「OK」をクリックします。  
   <img src="images/01_create_enviroment/2.3.2.png" width="350"> 

3. Welcomeで「Next」をクリックします。  
   <img src="images/01_create_enviroment/2.3.3.png" width="350"> 

4. Install Typeで「Standard」にチェックを入れて「Next」をクリックします。  
   <img src="images/01_create_enviroment/2.3.4.png" width="350"> 

5. Select UI Themeで、使用したいテーマを選択します。  
   テーマはどちらでも問題ございません。  
   テーマを選択後、「Next」をクリックします。  
   <img src="images/01_create_enviroment/2.3.5.png" width="350"> 

6. Verify Settingsで「Finish」をクリックします。  
   <img src="images/01_create_enviroment/2.3.6.png" width="350">

7. SDK Components Setupで、SDKのダウンロードの完了を待ちます。  
   <img src="images/01_create_enviroment/2.3.7.png" width="350">

8. Downloading Componentsでダウンロード完了後、「Finish」をクリックします。  
   <img src="images/01_create_enviroment/2.3.8.png" width="350">

---

### **2.4.adbの環境変数を設定** 

1. SDKのPathを確認します
   1. Android Studioを起動し、「Configure」をクリックします。  
   <img src="images/01_create_enviroment/2.5.1.1.png" width="350">  

   2. 「Settings」をクリックします。  
   <img src="images/01_create_enviroment/2.5.1.2.png" width="350">  

   3. Appearance & Behavior > Sysstem Settings > Android SDKを選択します。  
      その後、Android Locationをメモ帳にコピーします。  
   <img src="images/01_create_enviroment/2.5.1.3.png" width="350">  

   4. メモ帳にコピーしたAndroid Locationの後ろに、「\platform-tools」を追記します。  
      このPathが、`adbのpath`です。  
      例) C:\Users\User\AppData\Local\Android\Sdk\platform-tools  

      Pathが正しい場合、ファイルエクスプローラーでアクセスすると、以下の画面が表示されます。  
   <img src="images/01_create_enviroment/2.5.1.4.png" width="350">  

2. 環境変数を設定

   手順は[2.1.JDKのインストール](#2.1.JDKのインストール)と同じです。

   1. コントローラーパネルを開きます。
   2. システムとセキュリティを選択します。
   3. システムの詳細設定を選択します。
   4. 環境変数を選択します。
   5. システム環境変数の一覧からPathをダブルクリックします。
   6. 「新規」をクリックし、adbのpathを入力します。
   7. 「OK」をクリックし、環境変数名の編集画面を閉じます。
   8. 「OK」をクリックし、環境変数画面を閉じます。
   9. 「適応」をクリックし、システムのプロパティに反映させます。

---

### **2.5.Emulatorの作成・実行**

参考資料は[こちら](https://qiita.com/yacchi1123/items/5849df8965de19818617)の「1-3. AVD Managerの設定」をご参照ください。

1. Emulatorを作成します。
   1. Android Studioを起動します。  
      <img src="images/01_create_enviroment/2.4.1.1.png" width="350">  

   2. 「Configure」をクリックします。  
      <img src="images/01_create_enviroment/2.4.1.2.png" width="350">  

   3. 「AVD Manager」をクリックします。  
      <img src="images/01_create_enviroment/2.4.1.3.png" width="350">  

   4. Your Virtual Devicesの画面で「Create Virtual Device...」をクリックします。  
      <img src="images/01_create_enviroment/2.4.1.4.png" width="350">  

   5. 作成したい任意のDevicesを選択します。  
      <img src="images/01_create_enviroment/2.4.1.5.png" width="350">  

   6. 「Next」をクリックします。  
      <img src="images/01_create_enviroment/2.4.1.6.png" width="350">  

    <!-- <div style="page-break-before:always"> -->

   7. System Imageの画面で、使用する`Android OS`のVersionを選択します。  
      <img src="images/01_create_enviroment/2.4.1.7.png" width="350">  
        ※ `VT-x is disabled in BIOS.`のメッセージが表示されている場合、以下の手順で仮想デバイスを有効にしてください。  
        　 参考資料は[こちら](https://www.sony.jp/support/vaio/products/manual/vpcx11/contents/03_rec/bios/04/04.html)をご参照ください。  

   8. Downloadを選択し、ダウンロードをクリックします。  
      <img src="images/01_create_enviroment/2.4.1.8.png" width="350">  

   9. SDKのダウンロード完了後にFinishをクリックします。  
      <img src="images/01_create_enviroment/2.4.1.8.png" width="350">  

   10. 「Next」をクリックします。  
       <img src="images/01_create_enviroment/2.4.1.9.png" width="350">  

   11. Verify Configurationの画面で`AVD Name`の欄に`端末名`を入力します。  
       その他の設定は必要に応じて変更してください。  
       <img src="images/01_create_enviroment/2.4.1.9.png" width="350">  

   12. 「Finish」をクリックしてEmulatorの作成を完了します。  
       <img src="images/01_create_enviroment/2.4.1.10.png" width="350">  

2. Emulatorを実行します。    
   1. Your Virtual Devicesの画面を表示します。  
    （上記手順の`1.~3.`で表示できます）  
      <img src="images/01_create_enviroment/2.4.2.1.png" width="350">  

   2. 起動したいEmulatorをダブルクリックします。  
      <img src="images/01_create_enviroment/2.4.2.2.1.png" width="350">  
      <img src="images/01_create_enviroment/2.4.2.2.2.png" height="350">  

※ AVDが起動できない場合はBIOSでVTを有効にして下さい。  
VAIOの場合: https://www.sony.jp/support/vaio/products/manual/vpcx11/contents/03_rec/bios/04/04.html

1. 本機の電源を入れる。
2. VAIOのロゴマークが表示されたらF2キーを押す。
3. BIOSセットアップ画面が表示されます。
4. BIOSセットアップ画面が表示されない場合は、F2キーを数回押してください。
5. ←または→キーで［Advanced］を選択する。
6. ［Intel(R) Virtualization Technology］または［Intel(R) VT］を選択してEnterキーを押し、「Enabled」にする。

---

<!-- ### **2.7.Emulatorでアプリの実行**

1. Emulatorを起動します。    
  
2. 画面を下から上にスワイプし、アプリの一覧を表示します。  
（端末毎にアプリの一覧の表示方法は異なります）  
<img src="images/01_create_enviroment/2.6.2.png" height="350"> 

2. インストールしたアプリをタップします。  
（MyAppをインストールしたので、MyAppをタップします）  
<img src="images/01_create_enviroment/2.6.3.png" height="350">   -->