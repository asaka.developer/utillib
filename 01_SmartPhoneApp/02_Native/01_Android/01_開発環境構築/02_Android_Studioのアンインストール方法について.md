# Android Studioのアンインストール方法について

## **1.通常のアンインストール方法**

- 1.コントロールパネルのAndroid Studioのアンインストールを選択する
- 2.`Android User Settings`にチェックを入れ、アンインストールする

---

## **2.Android Studioを完全に削除する方法**

[1.通常のアンインストール方法](#1.通常のアンインストール方法)を実施後に以下のファイルを削除する必要がある

```path
C:\Users\$User\.android
C:\Users\$User\.AndroidStudio4.0
C:\Users\$User\AppData\Local\Android （隠しファイル）
C:\Program Files\Android
C:\Program Files (x86)\Android
```