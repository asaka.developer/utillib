# **Androidアプリの開発環境・実行環境について**

Androidアプリ開発研修を行うにあたり、事前に開発環境をダウンロードお願い致しいます。

---

## **1.JDKのダウンロード**

1. [JDK](https://www.oracle.com/technetwork/java/javase/downloads/index.html)のダウンロード画面を開きます。    

2. JDK Downloadを選択します。  
    <img src="images/01_create_enviroment/2.1.1.1.png" width="350">

3. PCのOSに応じたファイルを選択します。  
    <img src="images/01_create_enviroment/2.1.1.2.png" width="350">  
    - Windows 64bit の場合: `Windows x64 Installer`のインストーラーファイルをダウンロード
    - macOS 64bit の場合: `macOS Installer`のインストーラーファイルをダウンロード

4. 規約に同意し、ダウンロードボタンをクリックします。  
    <img src="images/01_create_enviroment/2.1.1.3.png" width="350">  

---

<!-- <div style="page-break-before:always"> -->

## **2.Android_Studioのダウンロード**

1. [Android Studio](https://developer.android.com/studio?hl=ja)をダウンロード画面を開きます。  
<img src="images/01_create_enviroment/2.2.1.png" width="350">

2. 規約に同意し、ダウンロードボタンを押します。  
<img src="images/01_create_enviroment/2.2.2.png" width="350">