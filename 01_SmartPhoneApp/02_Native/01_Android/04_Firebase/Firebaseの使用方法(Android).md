# 注意点

## **Firebaesのプロジェクト作成（Android編）**
  
`以下はFirebaseの操作`    
`※Android StudioのAssiistantで表示されている情報は古いので、調べる時は公式サイトを参照すること！`
  
1. 下記にアクセスし、ユーザーを登録  
　 https://console.firebase.google.com/u/0/?hl=ja  
  
`以下はAndroid Studioの操作`  
  
2. Android Studioでプロジェクトを作成

3. Android StudioのTool → Firebaseから以下を行う  
　 ・Connect your app to Firebase（Firebaseのプロジェクト・またはアプリを作成）  
　 ・Add FCM to your app（アプリに`google-services.json`を作成）  

4. FirebaseMessagingServiceの継承クラスを作成する（通知を受け取るためのクラス）

5. manifestに手順4で作成したServiceを読み込むように修正する

6. 作成したAndroidアプリをInstallし、起動する  
　 発行されたTokenを一時的にメモする  
  
`以下はFirebaseの操作`  
  
7. [FCM](https://console.firebase.google.com/u/0/?hl=ja)の通知を作成する

8. 手順6でメモしたTokenを入力し、デバイステストを行う

9. デバイステストで問題無ければ、ユーザーセグメントから通知先のアプリを選択し、キャンペーンを作成する

---

## **実装例**

### **FirebaseMessagingService**

```kotlin
package com.example.mvvm_tigers_app

import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class CustomFirebaseMessagingService : FirebaseMessagingService() {

    /**
     * トークンを取得する
     *
     * @param token トークン
     */
    override fun onNewToken(token: String) {
        Log.d("CustomFirebaseMessagingService", "onNewToken: $token")
    }

    /**
     * プッシュ通知を受け取り、通知を表示する
     *
     * @param remoteMessage 通知のメッセージ
     */
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        super.onMessageReceived(remoteMessage)
        Log.d("CustomFirebaseMessagingService", "onMessageReceived")
        // プッシュ通知を表示する
        val title = remoteMessage.notification?.title ?: ""
        val text = remoteMessage.notification?.body ?: ""
        Log.d("CustomFirebaseMessagingService", "title: $title")
        Log.d("CustomFirebaseMessagingService", "text: $text")
    }

    override fun onDeletedMessages() {
        Log.d("CustomFirebaseMessagingService", "onDeletedMessages")
    }
}
```

### **AndroidManifest.xml**

```xml
<service
    android:name=".CustomFirebaseMessagingService"
    android:exported="false">
    <intent-filter>
        <action android:name="com.google.firebase.MESSAGING_EVENT"/>
    </intent-filter>
</service>
```

### **アプリのBuild.gradle**

```gradle
apply plugin: 'com.google.gms.google-services'
dependencies {
    // ※最新のVersionにすること
    implementation 'com.google.firebase:firebase-messaging:21.0.0'
}
```

### **プロジェクトのBuild.gradle**

```gradle
buildscript {
    dependencies {
        classpath 'com.google.gms:google-services:4.3.4'
    }
}
```

