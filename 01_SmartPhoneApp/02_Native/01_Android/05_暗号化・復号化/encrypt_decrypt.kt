import android.util.Base64
import javax.crypto.Cipher
import javax.crypto.spec.IvParameterSpec
import javax.crypto.spec.SecretKeySpec

object encrypt_decrypt {
    val KEY = "test"
    val ALGORITHM = "Blowfish"
    val MODE = "Blowfish/CBC/PKCS5Padding"
    val IV = "abcdefgh"
    /**
     * データを暗号化する
     *
     * @param decryptedData 復号化されたデータ
     * @return 暗号化されたデータ
     */
    private fun encrypt(decryptedData: String): String {
        val secretKeySpec = SecretKeySpec(KEY.toByteArray(), ALGORITHM)
        val cipher = Cipher.getInstance(MODE)
        cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, IvParameterSpec(IV.toByteArray()))
        val byteArray = cipher.doFinal(decryptedData.toByteArray())
        return Base64.encodeToString(byteArray, Base64.DEFAULT)
    }

    /**
     * データを復号化する
     *
     * @param encryptedData 暗号化されたデータ
     * @return 復号化されてデータ
     */
    private fun decrypt(encryptedData: String): String {
        val values = Base64.decode(encryptedData, Base64.DEFAULT)
        val secretKeySpec = SecretKeySpec(KEY.toByteArray(), ALGORITHM)
        val cipher = Cipher.getInstance(MODE)
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, IvParameterSpec(IV.toByteArray()))
        return String(cipher.doFinal(values))
    }
}