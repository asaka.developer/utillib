# TextViewの使い方

## **1.概要**

TextViewを使用すると、端末上で文字列を表示する事ができます。
ここではTextViewの使い方について説明します。

---

## **2.レイアウト**

Android Studioにはレイアウトを作成するための`LayoutEditor`が存在します。  
LayoutEditorを使用すると、Androidアプリのレイアウトを作成できます。  
LayoutEditorで作成すると、xmlファイルに自動的に記述されます。  
ただし、LayoutEditorを使用すると、意図しないコンポーネントの移動の位置を移動してしまう危険があるため注意が必要です。

<br>

### **2.1.レイアウトファイルを開く**

1. Android Studioでプロジェクトを作成
2. res/layout/activity_main.xmlを開く

<br>

### **2.2.レイアウトのエディタについて**

図）LayoutEditor  
<img src="images/05_text_view/1.png" width=700 />

| 番号 | 内容                                             |
| ---- | ------------------------------------------------ |
| ①    | 使用したいコンポーネントを選択する               |
| ②    | レイアウトのプレビュー                           |
| ③    | LayoutEditorのモードを切り替える                 |
| ④    | コンポーネントの詳細を入力する（id, 文字名など） |


<br>

図）xml  
<img src="images/05_text_view/2.png" width=700 /> 

| 番号 | 内容                                                                                                                                                           |
| ---- | -------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| ⑤    | レイアウトを記述しているxmlファイルです。<br>LayoutEditorを使用すると自動的に記述されます。<br>慣れてしまえば直接xmlを編集すると早くレイアウトを作成できます。 |
| ⑥    | LayoutEditorのモードを切り替える                                                                                                                               |

LayoutEditor, xml共に、やっていることは同じです。  
例）Textの文字列を変更 → Textを「Hellow World!」から「Sample」に変更すると、プレビューでは「Sample」と表示されます。

---

## **3.ソースコード**

### **3.1.ソースコードを開く**

プロジェクトツリーから以下を開く
- app
    - java
        - com.example.sampletextview (`※アプリ作成時に指定したパッケージ名`)
            - MainActivity.kt

### **3.2.ソースコードについて** 

<img src="images/05_text_view/3.png" width=700 /> 

| 番号 | 内容                                                                                                                 |
| ---- | -------------------------------------------------------------------------------------------------------------------- |
| ①    | 選択したソースコードファイル                                                                                         |
| ②    | ソースコードの中身<br>プロジェクト作成時に自動作成されている。<br>ここを編集することで、アプリに機能を実装できます。 |

<br>

### **3.3.実装例***

プロジェクト作成時に自動生成されたソースコードではacitivity_main.xmlのレイアウトを読み込んでいます。  
これを、ソースコードで生成したTextViewを表示するように修正します。

修正前
```kotlin
package com.example.sampletextview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Activityにレイアウトをセットしている
        setContentView(R.layout.activity_main)
    }
}
```

修正後
```kotlin
package com.example.sampletextview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // TextViewを作成し、文字列にはSampleを代入している
        val textView = TextView(this)
        textView.text = "Sample"
        // ActivityにTextをセットしている
        setContentView(textView)
    }
}
```

### **3.4.TextViewのメソッド**

TextViewには多くのメソッドが存在します。
メソッドを呼び出す事で、TextViewに反映できます。

一部）
| メソッド名   | 内容               |
| ------------ | ------------------ |
| setText      | 表示するテキスト名 |
| setTextSize  | テキストのサイズ   |
| setTextColor | テキストの色       |

<br>

実装例
```kotlin
package com.example.sampletextview

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val textView = TextView(this)
        textView.text = "Sample"
        textView.textSize = 16F
        textView.setTextColor(Color.GRAY)
        setContentView(textView)
    }
}
```

<div style="page-break-before:always"></div>

---

## **4.応用編**

[3.3.実装例](#3.3.実装例)での実装例はソースコードでTextViewを実装する方法を記述しています。
ソースコードではなく、Layoutで作成したTextViewも編集することができます。
Layoutで作成したTextViewを編集する場合、ActivityにセットしたLayoutにセットしているButtonのidを指定する必要があります。
以下の実装例です。

```kotlin
package com.example.sampletextview

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val textView = TextView(this).apply {
            text = "Sample"
            textSize = 16F
            setTextColor(Color.GRAY)
        }
        setContentView(textView)
    }
}
```