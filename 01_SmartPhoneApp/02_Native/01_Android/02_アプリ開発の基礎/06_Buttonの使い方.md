# Buttonの使い方

## **1.概要**

Buttonを使用すると、端末上でボタンを表示する事ができます。
ここではButtonの実装方法について説明します。

---

## **2.レイアウト**

### **2.1.レイアウトファイルを開く**

1. Android Studioでプロジェクトを作成
2. res/layout/activity_main.xmlを開く

<br>

### **2.2.レイアウトのエディタについて**

図）LayoutEditor  
<img src="images/06_button/1.png" width=700 />

| 番号 | 内容                                             |
| ---- | ------------------------------------------------ |
| ①    | 使用したいコンポーネントを選択する（Button）     |
| ②    | レイアウトのプレビュー                           |
| ③    | LayoutEditorのモードを切り替える                 |
| ④    | コンポーネントの詳細を入力する（id, 文字名など） |


<br>

図）xml  
<img src="images/06_button/2.png" width=700 /> 

| 番号 | 内容                                                                                                                                                           |
| ---- | -------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| ⑤    | レイアウトを記述しているxmlファイルです。<br>LayoutEditorを使用すると自動的に記述されます。<br>慣れてしまえば直接xmlを編集すると早くレイアウトを作成できます。 |
| ⑥    | LayoutEditorのモードを切り替える                                                                                                                               |

<br>

LayoutEditor, xml共に、やっていることは同じです。  
例）Textの文字列を変更 → Textを「BUTTON」から「Sample」に変更すると、プレビューでは「Sample」と表示されます。

<br>

---

## **3.ソースコード**

### **3.1.ソースコードを開く**

プロジェクトツリーから以下を開く
- app
    - java
        - com.example.sampletextview (`※アプリ作成時に指定したパッケージ名`)
            - MainActivity.kt

### **3.2.ソースコードについて** 

<img src="images/05_text_view/3.png" width=700 /> 

| 番号 | 内容                                                                                                                 |
| ---- | -------------------------------------------------------------------------------------------------------------------- |
| ①    | 選択したソースコードファイル                                                                                         |
| ②    | ソースコードの中身<br>プロジェクト作成時に自動作成されている。<br>ここを編集することで、アプリに機能を実装できます。 |

<br>

### **3.3.実装例***

プロジェクト作成時に自動生成されたソースコードではacitivity_main.xmlのレイアウトを読み込んでいます。  
これを、ソースコードで生成したButtonを表示するように修正します。

修正前
```kotlin
package com.example.sampletextview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Activityにレイアウトをセットしている
        setContentView(R.layout.activity_main)
    }
}
```

修正後
```kotlin
package com.example.sampletextview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Buttonを作成し、文字列にはSampleを代入している
        val button = Button(this)
        button.text = "Sample"
        // ActivityにTextをセットしている
        setContentView(button)
    }
}
```

### **3.4.Buttonのメソッド**

Buttonには多くのメソッドが存在します。
メソッドを呼び出す事で、Buttonに反映できます。

使用率の高いメソッド）
| メソッド名         | 内容                                       |
| ------------------ | ------------------------------------------ |
| setText            | 表示するテキスト名                         |
| setTextSize        | テキストのサイズ                           |
| setTextColor       | テキストの色                               |
| setOnClickListener | ボタンを押した時に行いたい処理をセットする |

<br>

実装例
```kotlin
package com.example.sampletextview

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val button = Button(this)
        button.text = "Sample"
        button.textSize = 16F
        button.setTextColor(Color.GRAY)
        // ボタンを押すと、ボタンのテキストを変更するようにしている
        button.setOnClickListener {
            button.text = "Buttonを押した"
        }
        setContentView(button)
    }
}
```

---

## **4.応用編**

### **4.1.Kotlinの機能を活かした記述方法**

[3.3.実装例](#3.3.実装例)での実装例は、Java寄りに記述しています。  
Kotlinでは以下のように記述を省略できます。

```kotlin
package com.example.sampletextview

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val button = Button(this).apply {
            text = "Sample"
            textSize = 16F
            setTextColor(Color.GRAY)
        }
        setContentView(button)
    }
}
```

### **4.2.Layoutで設定したButtonの変更方法**

[3.3.実装例](#3.3.実装例)での実装例はソースコードでButtonを実装する方法を記述しています。
ソースコードではなく、Layoutで作成したButtonも編集することができます。
Layoutで作成したボタンを変数する場合、ActivityにセットしたLayoutにセットしているButtonのidを指定する必要があります。
以下の実装例です。

```kotlin
package com.example.sampletextview

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // レイアウトをセットする
        setContentView(R.layout.activity_main)
        // レイアウト上のボタンを編集する
        button.apply {
            text = "Sample"
            textSize = 16F
            setTextColor(Color.GREEN)
        }
    }
}
```