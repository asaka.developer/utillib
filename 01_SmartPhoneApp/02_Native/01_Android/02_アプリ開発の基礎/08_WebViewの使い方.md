# WebViewの使い方

## **1.概要**

WebViewを使用すると、Webサイトをアプリ上で表示することができます。
また、WebViewの動作を監視し、特定のURLにアクセスするときのみ、特定の処理を行うといった制御も可能です。

---

## **2.LayoutEditorを使用した実装**

LayoutEditorからWebViewをドラッグ&ドロップし、配置することが出来ます。  
<img src="images/08_webview/1.png" width=350 />

---

## **3.xml**

LayoutEditorでWebViewを配置すると、Layoutファイルにはコードが実装されます。  
そこに、`android:id="@+id/web_view"`を追記してください。  
このidは、ConstraintLayoutの制約や、ソースコードでの指定など、様々な用途で使用されます。  
`※WebViewに限らず、どのViewやLayoutでも同様です。`

```xml
<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <WebView
        android:id="@+id/web_view"
        android:layout_width="409dp"
        android:layout_height="220dp"
        tools:layout_editor_absoluteX="1dp"
        tools:layout_editor_absoluteY="241dp"
        tools:ignore="MissingConstraints" />

</androidx.constraintlayout.widget.ConstraintLayout>
```

---

## **4.ソースコードでWebViewを制御**

上記のxmlで設定したWebViewの`id`を指定することで、ソースコードからWebViewを呼び出すことができます。  
以下のようにWebViewのインスタンスを使用することで、WebViewの動作を制御できます。  

```kotlin
package com.example.sampletextview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebViewClient
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        web_view.webViewClient = object : WebViewClient() {
            // ここでWebViewの動作を監視できる
        }
        // WebViewでJavascriptを有効にする
        web_view.settings.javaScriptEnabled = true
        // WebViewでDomStorageを有効にする
        web_view.settings.domStorageEnabled = true
        // WebViewで表示するURLを指定する
        web_view.loadUrl("https://m.hanshintigers.jp/")
    }
}
```

---

## **5.権限の追加**

上記の方法でWebViewの実装が出来ました。  
しかし、このままですと動作しません。アプリ自体にインターネットを使用する権限が付与されていないからです。  
以下のように`AndroidManifest.xml`に`<uses-permission android:name="android.permission.INTERNET"/>`を追記することで、アプリにインターネットの権限を付与できます。

<img src="images/08_webview/2.png" width=500 />

---

## **6.備考**

LayoutEditorを使用せずに、ソースコードでWebViewを作成することができます。  
WebViewに限らず、LayoutEditorで行える事はソースコードのみでも実現可能です。  

```kotlin
package com.example.sampletextview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val webView = WebView(this).apply {
            webViewClient = object : WebViewClient() {
                // ここでWebViewの動作を監視できる
            }
            // WebViewでJavascriptを有効にする
            settings.javaScriptEnabled = true
            // WebViewでDomStorageを有効にする
            settings.domStorageEnabled = true
            // WebViewで表示するURLを指定する
            loadUrl("https://m.hanshintigers.jp/")
        }
        setContentView(webView)
    }
}
```