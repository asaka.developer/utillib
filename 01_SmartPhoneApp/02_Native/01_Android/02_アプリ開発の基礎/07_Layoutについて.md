# Layoutについて

## **1.概要**

ここではLayoutについて記述致します。
Androidには多くのLayoutが存在し、Layout上にWidget(TextView, Buttonなど)を配置することでUIを作成します。
この資料ではレスポンシブUIを実現するために、ConstraintLayoutについて記述致します。

---

## **2.ConstraintLayout**

ConstraintLayoutはレスポンシブUIを作成する際に使用します。  
Android端末はメーカーや機種により、画面のサイズは千差万別です。  
そのため、どの画面サイズでも適切なUI表示を実現する必要があります。  
より詳細な情報につきましては[公式ホームページ](https://developer.android.com/training/constraint-layout?hl=ja)をご確認下さい。

### **2.1.制約**

ConstraintLayoutは制約により位置情報が確定されます。
位置の指定方法は左右の比率で指定します。

例）真ん中の場合： 縦は1:1 横は1:1  
  　右寄せの場合： 縦は1:1 横は2:1  

<img src="images/07_constraint_layout/3.png" width=200 />

<br>

### **2.2.xml**

```xml
<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">

    <TextView
        android:id="@+id/textView"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="Hello World"
        app:layout_constraintBottom_toTopOf="@id/button"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent" />

    <Button
        android:id="@+id/button"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="Button"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintHorizontal_bias="0.35"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toBottomOf="@+id/textView" />

</androidx.constraintlayout.widget.ConstraintLayout>
```

---

## **備考**

LayoutにはConstraintLayout以外に、以下のレイアウトが存在します
- LinearLayout
- RelativeLayout
- GridLayout
- TableLayout

Layoutは奥が深いため、1つのドキュメントでは到底説明しきれません。  
そのため、より詳細な情報につきましては[公式ホームページ](https://developer.android.com/guide/topics/ui/declaring-layout?hl=ja)をご確認下さい。