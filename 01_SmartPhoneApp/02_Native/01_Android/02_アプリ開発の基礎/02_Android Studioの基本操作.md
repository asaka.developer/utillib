# Android Studioの基本操作

## **1.Android Studioのボタン**

Android Studioで使用頻度が特に多い項目について記述する  

<img src="images/02_how_to_use/1.png" width="700" /> 

| 番号 | 項目         | 内容                                    |
| ---- | ------------ | --------------------------------------- |
| 1    | アプリの実行 | アプリをBuild、及び実行する             |
| 2    | デバッグ     | アプリをBuild、及びデバッグ実行する     |
| 3    | アプリの停止 | アプリを停止する                        |
| 4    | Sync Project | Gradleに記述した内容をProjectに反映する |
| 5    | AVD Manager  | エミュレータの管理を行う                |
| 6    | SDK Manager  | SDKの管理を行う                         |
| 7    | Project Tree | プロジェクトの中身を表示する            |
| 8    | Terminal     | Terminalを表示する                      |
| 9    | Build        | Build状況を表示する                     |
| 10   | Logcat       | アプリの実行状況を表示する              |
| 11   | TODO         | 実装中のTODO, FIXMEの一覧を表示する     |
| 12   | 処理状況     | Sync, Buildの進捗を表示する             |

---

## **2.Android Studioのプロジェクトツリー**

Android Studioで使用頻度が特に多いファイルについて記述する  
※より詳細な内容が知りたい場合、[Android公式ホームぺージ](https://developer.android.com/guide/topics/manifest/manifest-intro?hl=ja)を参照

<img src="images/02_how_to_use/2.png" width="700" /> 

| 番号 | 項目                      | 内容                                                    |
| ---- | ------------------------- | ------------------------------------------------------- |
| 1    | AndroidManifest.xml       | Androidアプリの権限を定義する<br>その他の複数の定義あり |
| 2    | MainActivity.kt           | Androidアプリの処理を実装する                           |
| 3    | activity_main.xml         | Androidアプリのレイアウト（UI）を作成する               |
| 4    | build.gradle              | 外部ライブラリの読み込みや、Build Versionのせ設定を行う |
| 5    | タブ(AndroidManifest.xml) | AndroidManifest.xmlのエディターを表示する               |
| 6    | タブ(MainActivity.kt)     | MainActivity.ktのエディターを表示する                   |
| 7    | タブ(activity_main.xml)   | activity_main.xmlのエディターを表示する                 |
| 8    | タブ(build.gradle)        | build.gradleのエディターを表示する                      |

---

## **3.キーボード ショートカット**

Android Studioで使用頻度が特に多いキーボード ショートカットについて記述する  
※より詳細な内容が知りたい場合、[Android公式ホームぺージ](https://developer.android.com/studio/intro/keyboard-shortcuts?hl=ja)を参照

| キー                      | 内容                                                                              |
| ------------------------- | --------------------------------------------------------------------------------- |
| Ctrl + マウスの右クリック | 処理の呼び出し元、呼び出し先にジャンプする                                        |
| Alt + Enter               | 自動インポートする                                                                |
| Ctrl + Shift + F          | プロジェクト内のファイルを全検索する                                              |
| Ctrl + N                  | クラスファイルを検索する                                                          |
| Shift + F6                | ファイル名や変数名を変更し、プロジェクト全体に反映させる<br>※F2での名前変更は厳禁 |
| Ctrl + F9                 | Buildする                                                                         |
| Ctrl + F10                | Build + アプリを実行する                                                          |
| F9                        | デバッグ実行中に押すと、次のブレークポイントまで移動する                          |

---

## **4.備考**

その他、より詳細な内容が知りたい場合、[Android公式ホームぺージ](https://developer.android.com/guide?hl=ja)を参照
