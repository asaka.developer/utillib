# ButtonとWebViewを使用した練習問題

## **1.概要**

今までの復習を兼ねて、ボタンを押した際にTextViewの表示変更、及びWebViewの表示変更を実施します。

---

## **2.インターネットの権限を付与**

### **2.1.問題**

AndroidManifest.xmlに`<uses-permission android:name="android.permission.INTERNET" />`を追記し、アプリにインターネットの権限を付与してください。

### **2.2.回答**

AndroidManifest.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="com.example.myapplication">

    <uses-permission android:name="android.permission.INTERNET" />
    <application
        android:allowBackup="true"
        android:icon="@mipmap/ic_launcher"
        android:label="@string/app_name"
        android:roundIcon="@mipmap/ic_launcher_round"
        android:supportsRtl="true"
        android:theme="@style/AppTheme">
        <activity android:name=".MainActivity">
            <intent-filter>
                <action android:name="android.intent.action.MAIN" />

                <category android:name="android.intent.category.LAUNCHER" />
            </intent-filter>
        </activity>
    </application>

</manifest>
```

<div style="page-break-before:always"></div>

---

## **3.画面の作成**

### **3.1.問題**

以下の画面を作成してください。  
また、WebView, Buttonのidは以下を設定ください。  

| View                   | id                    |
| ---------------------- | --------------------- |
| WebViewのid            | `web_view`            |
| スタジアムナビのボタン | `stadium_navi_button` |
| チケットのボタン       | `ticket_button`       |
| 球団ニュースのボタン   | `news_button`         |
| 試合日程のボタン       | `date_button`         |
| リプレイのボタン       | `replay_button`       |
| SHOPのボタン           | `t_shop_button`       |

<br>

完成図  
<img src="images/09_2_practice_webview/2.1.png" width=300 />  


### **3.2.回答**

activity_main.xml

```xml
<?xml version="1.0" encoding="utf-8"?>
<androidx.constraintlayout.widget.ConstraintLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MainActivity">
    <WebView
        android:id="@+id/web_view"
        android:layout_width="0dp"
        android:layout_height="0dp"
        app:layout_constraintTop_toTopOf="parent"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintEnd_toEndOf="parent" />

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical">
        <Button
            android:id="@+id/stadium_navi_button"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="スタジアムナビ" />

        <Button
            android:id="@+id/ticket_button"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="チケット" />

        <Button
            android:id="@+id/news_button"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="球団ニュース" />

        <Button
            android:id="@+id/date_button"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="試合日程" />

        <Button
            android:id="@+id/replay_button"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="リプレイ" />

        <Button
            android:id="@+id/t_shop_button"
            android:layout_width="wrap_content"
            android:layout_height="wrap_content"
            android:text="T-SHOP" />
    </LinearLayout>
</androidx.constraintlayout.widget.ConstraintLayout>
```

<div style="page-break-before:always"></div>

---

## **4.ボタンを押したときに、WebViweを編集する**

### **4.1.問題**

ボタンを押したときに、以下の動作が行えるように実装してください。

| ボタン名                 | 処理の内容                                                                                   |
| ------------------------ | -------------------------------------------------------------------------------------------- |
| `スタジアムナビ`のボタン | ボタンを押すと、スタジアムナビを表示する。<br>`https://m.hanshintigers.jp/stadiumnavi/`      |
| `チケット`のボタン       | ボタンを押すと、チケットを表示する。<br>`https://m.hanshintigers.jp/ticket/`                 |
| `球団ニュース`のボタン   | ボタンを押すと、球団ニュースを表示する。<br>`https://m.hanshintigers.jp/column_news/`        |
| `試合日程`のボタン       | ボタンを押すと、試合日程を表示する。<br>`https://m.hanshintigers.jp/game/schedule/`          |
| `リプレイ動画`のボタン   | ボタンを押すと、リプレイ動画を表示する。<br>`https://m.hanshintigers.jp/stadiumnavi/replay/` |
| `T-SHOP`のボタン         | ボタンを押すと、T-SHOPを表示する。<br>`https://shop.hanshintigers.jp/`                       |

### **4.2.回答**

MainActivity.kt

```kotlin
package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebViewClient
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initWebView()
        initButtonListener()
    }

    /**
     * WebViewを初期化する
     */
    private fun initWebView() {
        web_view.webViewClient = WebViewClient()
        web_view.settings.javaScriptEnabled = true
        web_view.settings.domStorageEnabled = true
        web_view.settings.userAgentString = "Mozilla/5.0 (Linux; Android 9; S2 Build/5.280VE.81.a; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/73.0.3683.90 Mobile Safari/537.36 Mobile TigersApp"
    }

    /**
     * ボタンを押した時の処理を初期化する
     */
    private fun initButtonListener() {
        // ボタンを押すと、スタジアムナビを表示する
        stadium_navi_button.setOnClickListener {
            web_view.loadUrl("https://m.hanshintigers.jp/stadiumnavi/")
        }
        // ボタンを押すと、チケットを表示する
        ticket_button.setOnClickListener {
            web_view.loadUrl("https://m.hanshintigers.jp/ticket/")
        }
        // ボタンを押すと、球団ニュースを表示する
        news_button.setOnClickListener {
            web_view.loadUrl("https://m.hanshintigers.jp/column_news/")
        }
        // ボタンを押すと、試合日程を表示する
        date_button.setOnClickListener {
            web_view.loadUrl("https://m.hanshintigers.jp/game/schedule/")
        }
        // ボタンを押すと、リプレイ動画を表示する
        replay_button.setOnClickListener {
            web_view.loadUrl("https://m.hanshintigers.jp/stadiumnavi/replay/")
        }
        // ボタンを押すと、T-SHOPを表示する
        t_shop_button.setOnClickListener {
            web_view.loadUrl("https://shop.hanshintigers.jp/")
        }
    }
}
```