# Kotlinの基礎_練習

## **変数・定数の利用**

### **TextViewに文字列を代入**

```kotlin
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val str = "テキスト"
        val textView = TextView(this)
        textView.text = str
        setContentView(textView)
    }
}
```

### **TextViewに数字を代入**

```kotlin
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var number = 100
        number += 10
        val textView = TextView(this)
        textView.text = number.toString()
        setContentView(textView)
    }
}
```

### **ログを出力**

```kotlin
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val str = "テキスト"
        Log.d("Text", str)
        val number = 100
        Log.d("Number", "$number")
    }
}
```

---

## **Null**

```kotlin
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Null許容
        val textView: TextView? = TextView(this)

        // Nullの変数からはメソッドを呼びだせないので、Null非許容にして変数を呼びだせるようにする。
        textView?.text = "テキスト"
        textView?.textSize = 20F

        setContentView(textView)
    }
}
```

---

## **let**

```kotlin
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        // Null許容
        val textView: TextView? = TextView(this)

        // Nullの変数からはメソッドを呼びだせないので、Null非許容にして変数を呼びだせるようにする。
        textView?.let {
            it.text = "テキスト"
            it.textSize = 20F
        }
        setContentView(textView)
    }
}
```

---

## **if文**

```kotlin
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val isText = true
        val textView = TextView(this)
        if (isText) {
            textView.text = "真"
        } else {
            textView.text = "偽"
        }
        setContentView(textView)
    }
}
```

---

## **for文**

```kotlin
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // 0～99の数字をLogで表示する        
        for (number in 0..100) {
            Log.d("number", "$number")
        }
    }
}
```

### **配列使用パターン**

```kotlin
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // 0～99の数字をLogで表示する        
        for (number in 0..100) {
            Log.d("number", "$number")
        }
    }
}
```

---

## **while文**

```kotlin
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var number = 0
        while (number < 1000 ) {
            Log.d("number", "$number")
            number++
        }
    }
}
```

## **メソッド**

```kotlin
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val textView = TextView(this)
        textView.text = "変更前のテキスト"
        setContentView(textView)
        // テキストを変更する
        changeText(textView)
    }

    /**
     * テキストを変更する
     * （メソッド）
     * 
     * @param textView 変更前のTextView
     */
    private fun changeText(textView: TextView) {
        textView.text = "変更後のテキスト"
    }
}
```

---

## **クラス**

```kotlin
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val textView = TextView(this)
        textView.text = "変更前のテキスト"
        setContentView(textView)

        // テキストを変更する
        var number = 1
        // ログで出力
        Log.d("数値", "$number")

        // PlusNumberクラスのインスタンスを作成
        val plusNumber = PlusNumber()
        // PlusNumberクラスのplusメソッドを呼び出す
        number = plusNumber.plus(number)
        // ログで出力
        Log.d("数値", "$number")
    }
}
```