# Kotlinの基礎

## **コメント**

コメントを使用することで、プログラムにコメントを記述できます。  
コメントはプログラムとして扱われません。  
そのため、メモ書きとして記述する事が可能です。

`//`の後ろに記述する内容がコメントです。  
`/**/`や`/****/`もコメントです。

```kotlin
// これはコメントです。
/* これはコメントです */
/**
これはコメントです
**/
```

---

## **変数**

変数は何度でも値を変更することが出来ます。  
以下はよく使用される変数の一覧です。

| 型      | 内容                 |
| ------- | -------------------- |
| Int     | 整数                 |
| Float   | 小数                 |
| Double  | 小数                 |
| Boolean | 真・偽               |
| Char    | 文字                 |
| String  | 文字列               |
| List    | 配列（コレクション） |
| Array   | 配列（プリミティブ） |

<!--    | 型                   | 内容                                       | 値 |
| ------- | -------------------- | ------------------------------------------ |
| Int     | 整数                 | 32ビット整数 -2,147,483,648～2,147,483,647 |
| Float   | 小数                 | 32ビット単精度浮動小数点数                 |
| Double  | 小数                 | 64ビット倍精度浮動小数点数                 |
| Boolean | 真・偽               | true or false                              |
| Char    | 文字                 | 16ビットUnicode文字 ¥u0000～¥uFFFF         |
| String  | 文字列               |                                            |
| List    | 配列（コレクション） |                                            |

| Array   | 配列（プリミティブ） |                                            |
 -->

---

## **宣言**

```kotlin
// 文字列
var str: String?
// 数値(整数)
var number1: Int?
// 数値(小数)
var number2: Float?
```

---

## **代入**

```kotlin
// 文字列を代入
str = "test"
// 数値(整数)を代入
number1 = 1
// 数値(小数)を代入
number2 = 1.0
```

---

## **Null**

KotlinにはNull許容(Nullable)、Null非許容(NonNull)の2種類の型が存在します。  
Null非許容(NonNull)の型にはnullは代入出来ません。

Kotlinは`デフォルトではNull非許容`です。  
そのため、`nullを許可したい場合は明示的にNull許容`にする必用があります。  
変数の型に`?`を付けることで、Null許容となります。  
※Null許容とNull非許容は、別の型として扱われます。  

```kotlin
// Null許容
var str: String? = null
```

---

## **let**

letを使用することで、変数の中身を取り出すことができます。  
`?.let`を使用する事でNull許容の型から、Null非許容の型を取り出すときに使用する事が多いです。

```kotlin
// null許容の文字列
val str: String = "hellow"
str?.let { it ->
    // itがNull非許容の型となります。
}

```

---

## **定数**

定数は最初の1回しか値を変更できません（初期化時のみ）  
以降、値を代入するとコンパイルエラーが発生します。

```kotlin
// 文字列
val str: String = "test"
// 数値(整数)
val number1: Int = 1
// 数値(小数)
val number2: Float = 1.0
```

---

## **配列**

配列は、1つの型の値を複数を持ちます。  
配列を使用することで、同じ用途の目的の変数を1箇所にまとめる事ができます。

```kotlin
val array = arrayOf("1", "2", "3")
```

---

## **if文**

```kotlin
val number1 = 0
val number2 = 1
if (number1 == number2) {
    // Trueならこの処理を通る
} else {
    // Falseならこの処理を通る
}
```
---

## **for文**

```kotlin
// iが0~10の間、繰り返す
for (i in 0..10) {
    Log.d("for", "$i}")
}
```

### **配列との組み合わせ**

```kotlin
val array = arrayOf("1", "2", "3")
// 配列の要素の数だけ繰り返す
array.forEach {
    Log.d("forEach", "$it")
}
```
---

## **while**

```kotlin
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var number = 0
        while (number < 1000 ) {
            Log.d("number", "$number")
            number++
        }
    }
}
```

---

## **関数（メソッド）**

関数は、処理の役割毎に分類します。  
Androidの`Activity`では、起動時に`onCreate`内に処理を記述します。  
その際に、onCreate内に処理を全て記述すると、処理が分からなくなります。  
そのため、関数を使用し処理を分散することで、分かりやすく柔軟性のあるプログラムになります。  

関数は`fun 関数名(引数): 戻り値 {}`で定義する。  
戻り値は`return 値`で記述する。  

```kotlin
// 戻り値が無い関数
fun test() {
}

// 戻り値あがある関数
fun test1(): Int {
    // 戻り値
    return 0
}

```

---

## **クラス**

クラスは大枠の機能毎に作成します。  
1つのクラスに全ての処理を実装すると、膨大な処理数となり、ソースコードの可読性や柔軟性が失われます。  
そのため、不具合が多発し、処理も集約しているため修正が出来ない状態になってしまいます。  
大枠の機能毎にクラス分けをすることで、分かりやすく柔軟性のあるプログラムになります。  

```kotlin
class Test {
    fun test() {

    }
}
```

クラス内の処理を呼び出す際はインスタンスを作成し、インスタンス経由で呼び出します。
```kotlin
// Testクラスのインスタンスを作成
val test = Test()
// Testクラスのtest関数を呼び出す
test.test()
```

---

## **継承**

継承を使用すると、継承元クラスの機能を使用することができます。
継承方法は継承先クラス名の後ろに、`: 継承元クラス`を記述します。

```kotlin
//     継承先のクラス   継承元のクラス
class CustomTextView: TextView {

}
```

---

## 備考

文字コード
- [https://seiai.ed.jp/sys/text/java/utf8table.html](https://seiai.ed.jp/sys/text/java/utf8table.html)

Kotlinの基礎
- [https://qiita.com/SYABU555/items/7cc5909c119ec9ab0288](https://qiita.com/SYABU555/items/7cc5909c119ec9ab0288)

Kotlinの詳細な使い方
- [https://qiita.com/k5n/items/cc0377b75d8537ef8a85](https://qiita.com/k5n/items/cc0377b75d8537ef8a85)

